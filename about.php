<?php
// *************************************************************************
// *                                                                       *
// * DEPRIXA -  Integrated Web system                                      *
// * Copyright (c) JAOMWEB. All Rights Reserved                            *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: osorio2380@yahoo.es                                            *
// * Website: http://www.jaom.info                                         *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.                              *
// * If you Purchased from Codecanyon, Please read the full License from   *
// * here- http://codecanyon.net/licenses/standard                         *
// *                                                                       *
// *************************************************************************

  define("_VALID_PHP", true);
  require_once("init.php");
  

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!--Meta-->
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="Courier DEPRIXA-Integral Web System" />
    <meta name="author" content="Jaomweb">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--Favicon-->
    <link rel="icon" href="uploads/favicon.png">

    <!-- Title-->
    <title>About Us | <?php echo $core->site_name;?></title>
    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700%7COpen+Sans:400,600,700" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <!--Icon fonts-->
    <link rel="stylesheet" href="assets-theme/vendor/strokegap/style.css">
    <link rel="stylesheet" href="assets-theme/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets-theme/vendor/linearicons/style.css">
    <link rel="stylesheet" href="assets-theme/css/bundle.css">
    <link rel="stylesheet" href="assets-theme/css/style.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- AOS -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- aos -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
    window.projectServices = {
        ASSETS_ENV: "prod",
        brand: "safm",
        version: "stable",
        disableOpacityChange: true
    };
    </script>
    <!-- Facebook Pixel Code -->
    <script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2458420900951341');
    fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2458420900951341&ev=PageView&noscript=1(44 B)
	https://www.facebook.com/tr?id=2458420900951341&ev=PageView&noscript=1
	" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    <script type="text/javascript" src="assets.maerskline.com/integrated-global-nav/2/loader.js"></script>
</head>

<body id="top">

    <!--headers-->
    <header class="header header-shrink header-inverse fixed-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg px-md-0">

                <?php require_once("header.php");?>

            </nav>
        </div> <!-- END container-->
    </header> <!-- END header -->

    <div data-dark-overlay="6" data-init="parallax" class="u-py-30 u-pt-lg-200 u-pb-lg-50 u-flex-center"
        style="background:url(assets-theme/img/about/hero.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center text-white">
                    <h1 class="text-white"><span class="eng">About Us</span>
                        <span class="hin" style="display:none;">
                            हमारे बारे में</span>
                        <span class="arabic" style="display:none;">معلومات عنا</span>
                        <span class="indonesian" style="display:none;">Tentang kami</span>
                        <span class="french" style="display:none;">
                            À propos de nous</span>
                        <span class="ban" style="display:none;">
                            আমাদের সম্পর্কে</span>
                        <span class="italian" style="display:none;">
                            Riguardo a noi</span>
                        <span class="spanish" style="display:none;">Sobre nosotras</span>
                        <span class="german" style="display:none;">Über uns</span>
                        <span class="polish" style="display:none;">
                            O nas</span>
                        <span class="portuguese" style="display:none;">Sobre nós</span>
                        <span class="mandrin" style="display:none;">关于我们</span></h1>
                    <div class="u-h-4 u-w-50 bg-white rounded mx-auto my-4"></div>
                    <p class="lead">

                    </p>
                </div>
            </div> <!-- END row-->
        </div> <!-- END container-->
    </div> <!-- END intro-hero-->
    <section class="sec-bg-img-set">
        <div class="container">
            <div class="row align-items-center pt-5">

                <div class="col-lg-5 mb-4 mr-auto text-center" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                    data-aos-duration="2000">
                    <img class="wow fadeInLeft w-100 rounded" src="assets-theme/img/about/sc-1.jpg"
                        alt="post and parcel award">
                </div> <!-- END col-lg-6 pl-lg-5 -->

                <div class="col-lg-6">
                    <h2 class="h1 font--display-3" style="color:#004994;font-size:30px;">
                        <span class="eng">WHO WE ARE</span>
                        <span class="french" style="display:none;">
                            QUI NOUS SOMMES</span>
                        <span class="hin" style="display:none;">हम कौन हैं</span>
                        <span class="arabic" style="display:none;">من نحن</span>
                        <span class="indonesian" style="display:none;">
                            SIAPA KITA</span>
                        <span class="ban" style="display:none;">আমরা কারা</span>
                        <span class="italian" style="display:none;">CHI SIAMO</span>
                        <span class="spanish" style="display:none;">
                            QUIENES SOMOS</span>
                        <span class="german" style="display:none;">WER WIR SIND</span>
                        <span class="polish" style="display:none;">
                            KIM JESTEŚMY</span>
                        <span class="portuguese" style="display:none;">
                            QUEM NÓS SOMOS</span>
                        <span class="mandrin" style="display:none;">我们是谁</span>
                    </h2>
                    <div class="u-h-4 u-w-50 rounded mt-4 mb-5" style="background-color:#004994;"></div>
                    <p class="mb-5 text-white text-justify" data-aos="fade-up" data-aos-duration="3000">
                        <span class="eng">We are your personal shipper, and logistics management company that control
                            the efficient shipping, and storage of your goods. We also service, and relate information
                            between the point of origin and the point of consumption in order to meet your needs.</span>
                        <span class="french" style="display:none;">
                            Nous sommes votre expéditeur personnel et une société de gestion de la logistique qui
                            contrôle l'expédition et le stockage efficaces de vos marchandises. Nous assurons également
                            le service et relions les informations entre le point d'origine et le point de consommation
                            afin de répondre à vos besoins.</span>
                        <span class="hin" style="display:none;">हम आपके व्यक्तिगत शिपर, और लॉजिस्टिक्स मैनेजमेंट कंपनी
                            हैं जो आपके माल के कुशल शिपिंग, और भंडारण को नियंत्रित करते हैं। हम आपकी आवश्यकताओं को पूरा
                            करने के लिए उत्पत्ति के बिंदु और खपत के बिंदु के बीच जानकारी भी सेवा, और संबंधित करते
                            हैं।</span>
                        <span class="arabic" style="display:none;">نحن شركة الشحن الخاصة بك ، وشركة إدارة الخدمات
                            اللوجستية التي تتحكم في الشحن الفعال وتخزين البضائع الخاصة بك. نقوم أيضًا بالخدمة ، وربط
                            المعلومات بين نقطة المنشأ ونقطة الاستهلاك من أجل تلبية احتياجاتك.</span>
                        <span class="indonesian" style="display:none;">
                            Kami adalah pengirim pribadi Anda, dan perusahaan manajemen logistik yang mengontrol
                            pengiriman dan penyimpanan barang Anda secara efisien. Kami juga melayani, dan mengaitkan
                            informasi antara titik asal dan titik konsumsi untuk memenuhi kebutuhan Anda.</span>
                        <span class="ban" style="display:none;">আমরা আপনার ব্যক্তিগত শিপ এবং লজিস্টিক্স ম্যানেজমেন্ট
                            সংস্থা যা আপনার দক্ষ জাহাজীকরণ এবং আপনার পণ্য সংরক্ষণের জন্য নিয়ন্ত্রণ করে। আমরা আপনার
                            প্রয়োজনীয়তা মেটাতে সার্ভিসটিও সরবরাহ করি, এবং আপনার প্রয়োজনীয়তাগুলি পূরণের জন্য উত্স এবং
                            ব্যবহারের বিন্দুর মধ্যে তথ্য সম্পর্কিত করি।</span>
                        <span class="italian" style="display:none;">Siamo il vostro spedizioniere personale e la società
                            di gestione della logistica che controlla la spedizione e lo stoccaggio efficienti delle
                            vostre merci. Inoltre forniamo assistenza e mettiamo in relazione le informazioni tra il
                            punto di origine e il punto di consumo al fine di soddisfare le vostre esigenze.</span>
                        <span class="spanish" style="display:none;">
                            Somos su empresa de transporte personal y gestión de logística que controla el envío y el
                            almacenamiento eficientes de sus productos. También damos servicio y relacionamos
                            información entre el punto de origen y el punto de consumo para satisfacer sus
                            necesidades.</span>
                        <span class="german" style="display:none;">Wir sind Ihr persönlicher Versender und
                            Logistikmanagementunternehmen, das den effizienten Versand und die Lagerung Ihrer Waren
                            kontrolliert. Wir warten und verknüpfen auch Informationen zwischen dem Ursprungsort und dem
                            Verbrauchsort, um Ihre Bedürfnisse zu erfüllen.</span>
                        <span class="polish" style="display:none;">
                            Jesteśmy Twoim osobistym nadawcą i firmą zarządzającą logistyką, która kontroluje efektywną
                            wysyłkę i przechowywanie twoich towarów. Obsługujemy również i łączymy informacje między
                            punktem początkowym a punktem konsumpcji, aby spełnić Twoje potrzeby.</span>
                        <span class="portuguese" style="display:none;">
                            Somos o seu remetente pessoal e a empresa de gerenciamento de logística que controla o envio
                            e o armazenamento eficientes de suas mercadorias. Também prestamos serviços de manutenção e
                            relacionamos informações entre o ponto de origem e o ponto de consumo para atender às suas
                            necessidades.</span>
                        <span class="mandrin"
                            style="display:none;">我们是您的私人托运人和物流管理公司，可控制您货物的有效运输和存储。我们还提供服务并在起点和消费点之间提供信息，以满足您的需求。</span>
                    </p>
                    <ul class="list-unstyled u-fw-600 text-white">
                        <li class="d-flex align-items-center mb-2" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                            data-aos-duration="2000">
                            <!-- <span class="lnr lnr-checkmark-circle mr-2 color-primary u-fs-20"><i class="fas fa-dot-circle"></i></span> -->
                            <span><i class="fas fa-arrow-right"
                                    style="color:#004994;font-size:20px;"></i>&nbsp;&nbsp;</span>
                            <span class="eng">Professional and easy to communicate with.</span>
                            <span class="french" style="display:none;">
                                Professionnel et facile à communiquer.</span>
                            <span class="hin" style="display:none;">पेशेवर और साथ संवाद करने में आसान।</span>
                            <span class="arabic" style="display:none;">محترف وسهل التواصل معه.</span>
                            <span class="indonesian" style="display:none;">
                                Profesional dan mudah diajak berkomunikasi.</span>
                            <span class="ban" style="display:none;">পেশাদার এবং সাথে যোগাযোগ করা সহজ।</span>
                            <span class="italian" style="display:none;">Professionale e facile da comunicare.</span>
                            <span class="spanish" style="display:none;">
                                Profesional y fácil de comunicar.</span>
                            <span class="german" style="display:none;">Professionell und einfach zu
                                kommunizieren.</span>
                            <span class="polish" style="display:none;">
                                Profesjonalny i łatwy w komunikacji.</span>
                            <span class="portuguese" style="display:none;">
                                Profissional e fácil de se comunicar.</span>
                            <span class="mandrin" style="display:none;">专业且易于沟通。</span>
                        </li>
                        <li class="d-flex align-items-center mb-2" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                            data-aos-duration="2000">
                            <span><i class="fas fa-arrow-right"
                                    style="color:#004994;font-size:20px;"></i>&nbsp;&nbsp;</span>
                            <span class="eng">Discounted Carrier Rates.</span>
                            <span class="french" style="display:none;">
                                Tarifs réduits pour les opérateurs.</span>
                            <span class="hin" style="display:none;">रियायती कैरियर दरें।</span>
                            <span class="arabic" style="display:none;">أسعار الناقل المخفضة.</span>
                            <span class="indonesian" style="display:none;">
                                Tarif Pembawa Diskon.</span>
                            <span class="ban" style="display:none;">ছাড়ের বাহকের হারগুলি।</span>
                            <span class="italian" style="display:none;">Tariffe di trasporto scontate.</span>
                            <span class="spanish" style="display:none;">
                                Tarifas de transporte con descuento.</span>
                            <span class="german" style="display:none;">Ermäßigte Carrier-Preise.</span>
                            <span class="polish" style="display:none;">
                                Zdyskontowane stawki przewoźnika.</span>
                            <span class="portuguese" style="display:none;">
                                Tarifas de operadora com desconto.</span>
                            <span class="mandrin" style="display:none;">折扣的运营商费率。</span>
                        </li>
                        <li class="d-flex align-items-center mb-2" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                            data-aos-duration="2000">
                            <span><i class="fas fa-arrow-right"
                                    style="color:#004994;font-size:20px;"></i>&nbsp;&nbsp;</span>
                            <span class="eng">Customs & Compliance Inspection.</span>
                            <span class="french" style="display:none;">
                                Inspection des douanes et de la conformité.</span>
                            <span class="hin" style="display:none;">सीमा शुल्क और अनुपालन निरीक्षण।</span>
                            <span class="arabic" style="display:none;">التفتيش الجمركي والامتثال.</span>
                            <span class="indonesian" style="display:none;">
                                Inspeksi Kepabeanan & Kepatuhan.</span>
                            <span class="ban" style="display:none;">শুল্ক এবং সম্মতি পরিদর্শন।</span>
                            <span class="italian" style="display:none;">Ispezione doganale e di conformità.</span>
                            <span class="spanish" style="display:none;">
                                Inspección de Aduanas y Cumplimiento.</span>
                            <span class="german" style="display:none;">Zoll- und Compliance-Inspektion.</span>
                            <span class="polish" style="display:none;">
                                Kontrola celna i zgodność.</span>
                            <span class="portuguese" style="display:none;">
                                Inspeção aduaneira e de conformidade....</span>
                            <span class="mandrin" style="display:none;">海关合规检查</span>

                        </li>
                        <li class="d-flex align-items-center mb-2" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                            data-aos-duration="2000">
                            <span><i class="fas fa-arrow-right"
                                    style="color:#004994;font-size:20px;"></i>&nbsp;&nbsp;</span>
                            <span class="eng">Package Return Services Details.</span>
                            <span class="french" style="display:none;">
                                Détails des services de retour de colis.</span>
                            <span class="hin" style="display:none;">पैकेज रिटर्न सेवा विवरण।</span>
                            <span class="arabic" style="display:none;">تفاصيل حزمة خدمات العودة.</span>
                            <span class="indonesian" style="display:none;">
                                Rincian Layanan Pengembalian Paket.</span>
                            <span class="ban" style="display:none;">প্যাকেজ রিটার্ন পরিষেবাদির বিবরণ।</span>
                            <span class="italian" style="display:none;">Dettagli sui servizi di restituzione del
                                pacchetto.</span>
                            <span class="spanish" style="display:none;">
                                Detalles de los servicios de devolución de paquetes.</span>
                            <span class="german" style="display:none;">Details zum Paketrückgabedienst.</span>
                            <span class="polish" style="display:none;">
                                Szczegóły usługi zwrotu paczki.</span>
                            <span class="portuguese" style="display:none;">
                                Detalhes dos Serviços de Devolução do Pacote.</span>
                            <span class="mandrin" style="display:none;">包裹退回服务详细信息。</span>
                        </li>
                    </ul>
                </div> <!-- END col-lg-6 ml-auto-->
            </div> <!-- END row-->
        </div> <!-- END container-->
        <div class="border-img-middle"></div>
        <!-- <hr class="m-0"> -->

        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <h2 class="h1 font--display-3 pt-5" style="color:#004994;font-size:30px;">
                        <span class="eng">WHY YOU SHOULD CHOOSE US?</span>
                        <span class="french" style="display:none;">
						POURQUOI DEVRIEZ-VOUS NOUS CHOISIR?</span>
                        <span class="hin" style="display:none;">आप हमें क्यों चुन सकते हैं?</span>
                        <span class="arabic" style="display:none;">لماذا تختار الولايات المتحدة؟</span>
                        <span class="indonesian" style="display:none;">
						MENGAPA ANDA HARUS MEMILIH KAMI?</span>
                        <span class="ban" style="display:none;">আপনি কেন আমাদের বেছে নেবেন?</span>
                        <span class="italian" style="display:none;">PERCHÉ DOVREI SCEGLIERCI?</span>
                        <span class="spanish" style="display:none;">
						¿POR QUÉ DEBERÍAS ELEGIRNOS?</span>
                        <span class="german" style="display:none;">WARUM SOLLTEN SIE UNS WÄHLEN?</span>
                        <span class="polish" style="display:none;">
						DLACZEGO WARTO WYBRAĆ NAS?</span>
                        <span class="portuguese" style="display:none;">
						Por que você deve nos escolher?</span>
                        <span class="mandrin" style="display:none;">您为什么要选择我们？</span>
                    </h2>
                    <div class="u-h-4 u-w-50 rounded mt-4 mb-5" style="background-color:#004994;"></div>
                    <p class="text-white" data-aos="fade-up" data-aos-duration="3000">
						<span class="eng">At Bellshiptinow we are professionals and expert on maximizing members SAVING on shipping.</span>
                                <span class="french" style="display:none;">
								Chez Bellshiptinow, nous sommes des professionnels et des experts pour maximiser les membres ÉCONOMISANT sur l'expédition.</span>
                                <span class="hin" style="display:none;">Bellshiptinow में हम शिपिंग पर SAVING के सदस्यों को अधिकतम करने के लिए पेशेवर और विशेषज्ञ हैं।</span>
                                <span class="arabic" style="display:none;">في Bellshiptinow نحن محترفون وخبراء في تعظيم الأعضاء توفير في الشحن.</span>
                                <span class="indonesian" style="display:none;">
								Di Bellshiptinow, kami adalah profesional dan ahli dalam memaksimalkan anggota MENYIMPAN pengiriman.</span>
                                <span class="ban" style="display:none;">বেলশিটিনোতে আমরা পেশাদার এবং শিপিংয়ের উপর সাশ্রয়কারী সদস্যদের সর্বোচ্চকরণের বিশেষজ্ঞ।</span>
                                <span class="italian" style="display:none;">In Bellshiptinow siamo professionisti ed esperti per massimizzare i membri RISPARMIO sulla spedizione.</span>
                                <span class="spanish" style="display:none;">
								En Bellshiptinow somos profesionales y expertos en maximizar los miembros que AHORRAN en el envío.</span>
                                <span class="german" style="display:none;">Bei Bellshiptinow sind wir Profis und Experten für die Maximierung der Mitglieder.</span>
                                <span class="polish" style="display:none;">
								W Bellshiptinow jesteśmy profesjonalistami i ekspertami w zakresie maksymalizacji członków OSZCZĘDNOŚĆ na wysyłce.</span>
                                <span class="portuguese" style="display:none;">
								Na Bellshiptinow, somos profissionais e especialistas em maximizar os membros, economizando no transporte.</span>
                                <span class="mandrin" style="display:none;">在Bellshiptinow，我们是专业人士和专家，致力于最大限度地节省会员。</span>
                    </p>
                    <div class="media mt-4">
                        <span class="icon icon-Web u-fs-28 mr-3 mt-2" style="color:#004994;"></span>
                        <div class="media-body">
                            <h4 class="h5 font--display-3" style="color:#004994;font-size:20px;">
								<span class="eng">Packing service and Tip to SAVE on your shipping cost</span>
                                <span class="french" style="display:none;">
								Service d'emballage et astuce pour économiser sur vos frais de port</span>
                                <span class="hin" style="display:none;">पैकिंग सेवा और अपने शिपिंग लागत पर बचाने के लिए टिप</span>
                                <span class="arabic" style="display:none;">خدمة التعبئة و نصيحة للتوفير في تكلفة الشحن الخاصة بك</span>
                                <span class="indonesian" style="display:none;">
								Layanan pengepakan dan Tip untuk MENGHEMAT biaya pengiriman Anda</span>
                                <span class="ban" style="display:none;">প্যাকিং পরিষেবা এবং আপনার শিপিংয়ের ব্যয় সংরক্ষণ করতে টিপ</span>
                                <span class="italian" style="display:none;">Servizio di imballaggio e Suggerimento per risparmiare sul costo di spedizione</span>
                                <span class="spanish" style="display:none;">
								Servicio de embalaje y Sugerencia para AHORRAR en su costo de envío</span>
                                <span class="german" style="display:none;">Verpackungsservice und Tipp, um Ihre Versandkosten zu sparen</span>
                                <span class="polish" style="display:none;">
								Usługa pakowania i wskazówka, aby zaoszczędzić na kosztach wysyłki</span>
                                <span class="portuguese" style="display:none;">
								Serviço de embalagem e dica para economizar no seu custo de transporte</span>
                                <span class="mandrin" style="display:none;">包装服务和节省运费的提示</span>
                            </h4>
                            <p class="text-white text-justify" data-aos="fade-up" data-aos-duration="3000">
								<span class="eng">You can decrease your shipping cost by 80% when you send multiple packages because we
                                will consolidate them into one. We pack every item by hand in order to make sure your
                                valuable purchases are safely protected.</span>
                                <span class="french" style="display:none;">
								Vous pouvez réduire vos frais d'expédition de 80% lorsque vous envoyez plusieurs colis, car nous les regrouperons en un seul. Nous emballons chaque article à la main afin de nous assurer que vos précieux achats sont protégés en toute sécurité.</span>
                                <span class="hin" style="display:none;">जब आप कई पैकेज भेजते हैं तो आप अपनी शिपिंग लागत को 80% तक कम कर सकते हैं क्योंकि हम उन्हें एक में समेकित करेंगे। हम यह सुनिश्चित करने के लिए कि आपकी मूल्यवान खरीद सुरक्षित रूप से सुरक्षित है, हम प्रत्येक वस्तु को हाथ से पैक करते हैं।</span>
                                <span class="arabic" style="display:none;">يمكنك خفض تكلفة الشحن الخاصة بك بنسبة 80٪ عند إرسال حزم متعددة لأننا سنقوم بدمجها في حزمة واحدة. نقوم بحزم كل عنصر يدويًا للتأكد من حماية مشترياتك القيمة بأمان.</span>
                                <span class="indonesian" style="display:none;">
								Anda dapat mengurangi biaya pengiriman hingga 80% ketika Anda mengirim beberapa paket karena kami akan menggabungkannya menjadi satu. Kami mengemas setiap barang dengan tangan untuk memastikan pembelian Anda yang berharga terlindungi dengan aman.</span>
                                <span class="ban" style="display:none;">আপনি একাধিক প্যাকেজ পাঠানোর সময় আপনি আপনার শিপিংয়ের ব্যয়টি ৮০% হ্রাস করতে পারবেন কারণ আমরা সেগুলিকে একের মধ্যে একীভূত করব। আপনার মূল্যবান ক্রয়গুলি নিরাপদে সুরক্ষিত রয়েছে তা নিশ্চিত করার জন্য আমরা প্রতিটি আইটেম হাতে হাতে প্যাক করি।</span>
                                <span class="italian" style="display:none;">Puoi ridurre i costi di spedizione dell'80% quando invii più pacchi perché li raggrupperemo in uno solo. Imballiamo ogni articolo a mano per assicurarci che i tuoi preziosi acquisti siano protetti in modo sicuro.</span>
                                <span class="spanish" style="display:none;">
								Puede reducir su costo de envío en un 80% cuando envía múltiples paquetes porque los consolidaremos en uno. Empacamos cada artículo a mano para asegurarnos de que sus valiosas compras estén protegidas de forma segura.</span>
                                <span class="german" style="display:none;">Sie können Ihre Versandkosten um 80% senken, wenn Sie mehrere Pakete versenden, da wir sie zu einem zusammenfassen. Wir verpacken jeden Artikel von Hand, um sicherzustellen, dass Ihre wertvollen Einkäufe sicher geschützt sind.</span>
                                <span class="polish" style="display:none;">
								Możesz zmniejszyć koszt wysyłki o 80%, gdy wysyłasz wiele paczek, ponieważ skonsolidujemy je w jeden. Każdy przedmiot pakujemy ręcznie, aby zapewnić bezpieczeństwo Twoich cennych zakupów.</span>
                                <span class="portuguese" style="display:none;">
								Você pode diminuir o custo de remessa em 80% ao enviar vários pacotes, porque os consolidaremos em um. Nós embalamos todos os itens manualmente, para garantir que suas compras valiosas sejam protegidas com segurança.</span>
                                <span class="mandrin" style="display:none;">当您发送多个包裹时，您可以将运输成本降低80％，因为我们会将它们合并为一个包裹。我们会手工包装每件物品，以确保您的贵重物品得到安全保护。</span>
                            </p>
                        </div>
                    </div>
                    <div class="media mt-4">
                        <span class="icon icon-Bulb u-fs-28 mr-3 mt-2" style="color:#004994;"></span>
                        <div class="media-body">
                            <h4 class="h5 font--display-3" style="color:#004994;font-size:20px;">
								<span class="eng">A Productivity Platform</span>
                                <span class="french" style="display:none;">
								Une plateforme de productivité</span>
                                <span class="hin" style="display:none;">एक उत्पादकता मंच</span>
                                <span class="arabic" style="display:none;">منصة إنتاجية</span>
                                <span class="indonesian" style="display:none;">
								Platform Produktivitas</span>
                                <span class="ban" style="display:none;">একটি উত্পাদনশীলতা প্ল্যাটফর্ম</span>
                                <span class="italian" style="display:none;">Una piattaforma di produttività</span>
                                <span class="spanish" style="display:none;">
								Una plataforma de productividad</span>
                                <span class="german" style="display:none;">Eine Produktivitätsplattform</span>
                                <span class="polish" style="display:none;">
								Platforma produktywności</span>
                                <span class="portuguese" style="display:none;">
								Uma plataforma de produtividade</span>
                                <span class="mandrin" style="display:none;">生产力平台</span>
                            </h4>
                            <p class="text-white text-justify" data-aos="fade-up" data-aos-duration="3000">
								<span class="eng">We offer free package consolidation in order to SAVE our members money on international
                                shipping charges.</span>
                                <span class="french" style="display:none;">
								Nous offrons gratuitement la consolidation de colis afin d'économiser de l'argent sur les frais d'expédition internationaux de nos membres.</span>
                                <span class="hin" style="display:none;">हम अंतरराष्ट्रीय शिपिंग शुल्क पर हमारे सदस्यों के पैसे बचाने के लिए मुफ्त पैकेज समेकन प्रदान करते हैं।</span>
                                <span class="arabic" style="display:none;">نحن نقدم دمج مجاني للطرود من أجل توفير المال لأعضائنا في رسوم الشحن الدولية.</span>
                                <span class="indonesian" style="display:none;">
								Kami menawarkan konsolidasi paket gratis untuk MENYIMPAN uang anggota kami dengan biaya pengiriman internasional.</span>
                                <span class="ban" style="display:none;">আন্তর্জাতিক শিপিংয়ের জন্য আমাদের সদস্যদের অর্থ সাশ্রয় করার জন্য আমরা বিনামূল্যে প্যাকেজ একীকরণ প্রস্তাব করি।</span>
                                <span class="italian" style="display:none;">Offriamo il consolidamento gratuito dei pacchi per risparmiare i nostri membri sulle spese di spedizione internazionali.</span>
                                <span class="spanish" style="display:none;">
								Ofrecemos consolidación de paquetes gratis para AHORRAR a nuestros miembros dinero en gastos de envío internacionales.</span>
                                <span class="german" style="display:none;">Wir bieten eine kostenlose Paketkonsolidierung an, um unseren Mitgliedern Geld für internationale Versandkosten zu sparen.</span>
                                <span class="polish" style="display:none;">
								Oferujemy bezpłatną konsolidację paczek, aby zaoszczędzić naszym członkom pieniądze na międzynarodowych opłatach transportowych.</span>
                                <span class="portuguese" style="display:none;">
								Oferecemos consolidação gratuita de pacotes, a fim de economizar dinheiro de nossos membros com despesas de remessa internacional.</span>
                                <span class="mandrin" style="display:none;">我们提供免费的包裹合并服务，以便为我们的会员节省国际运费。</span>
                            </p>
                            </p>
                            <p class="u-fs-14 u-lh-1_8 my-4 text-justify font--display-3" style="color:#004994;">
                                ****<span class="eng">Bellshipitnow’s membership is free, customer will only pay for their shipping and
                                $35 administration/service fee of shipment items enclosed in a box with max dimensions
                                12x10x9, any additional boxes or anything exceeding these dimensions will not exceed
                                half of administration/service fee.</span>
                                <span class="french" style="display:none;">
								L'adhésion à Bellshipitnow est gratuite, le client ne paiera que son expédition et les frais d'administration / de service de 35 $ pour les articles expédiés enfermés dans une boîte de dimensions maximales 12x10x9, toute boîte supplémentaire ou tout élément dépassant ces dimensions ne dépassera pas la moitié des frais d'administration / de service.</span>
                                <span class="hin" style="display:none;">Bellshipitnow की सदस्यता मुफ़्त है, ग्राहक केवल अपनी शिपिंग के लिए भुगतान करेंगे और अधिकतम आयाम 12x10x9 के साथ बॉक्स में संलग्न शिपमेंट वस्तुओं का $ 35 प्रशासन / सेवा शुल्क, कोई भी अतिरिक्त बॉक्स या इन आयामों से अधिक कुछ भी प्रशासन / सेवा शुल्क के आधे से अधिक नहीं होगा।</span>
                                <span class="arabic" style="display:none;">عضوية Bellshipitnow مجانية ، ولن يدفع العميل إلا مقابل الشحن الخاص به و 35 دولارًا رسوم إدارية / خدمة لعناصر الشحن المرفقة في صندوق بأبعاد قصوى 12 × 10 × 9 ، ولن تتجاوز أي صناديق إضافية أو أي شيء يتجاوز هذه الأبعاد نصف رسوم الإدارة / الخدمة.</span>
                                <span class="indonesian" style="display:none;">
								Keanggotaan Bellshipitnow gratis, pelanggan hanya akan membayar biaya pengiriman dan biaya administrasi / layanan $ 35 untuk barang kiriman yang terlampir dalam kotak dengan dimensi maksimal 12x10x9, setiap kotak tambahan atau apa pun yang melebihi dimensi ini tidak akan melebihi setengah dari biaya administrasi / layanan.</span>
                                <span class="ban" style="display:none;">বেলশিপ্নো'র সদস্যতা নিখরচায়, গ্রাহক কেবল তাদের শিপিংয়ের জন্য এবং সর্বোচ্চ মাত্রা 12x10x9 সহ একটি বাক্সে আবদ্ধ জিনিসপত্রের চালনা আইটেমগুলির / 35 প্রশাসন / পরিষেবা ফি প্রদান করবেন, কোনও অতিরিক্ত বাক্স বা এই মাত্রা অতিক্রমকারী কোনও কিছুই প্রশাসন / পরিষেবা ফির অর্ধেকের বেশি হবে না।</span>
                                <span class="italian" style="display:none;">L'iscrizione a Bellshipitnow è gratuita, il cliente pagherà solo per le spese di spedizione e $ 35 di amministrazione / servizio degli articoli di spedizione racchiusi in una scatola con dimensioni massime 12x10x9, eventuali scatole aggiuntive o qualsiasi cosa che superi queste dimensioni non supererà la metà della tassa di amministrazione / servizio.</span>
                                <span class="spanish" style="display:none;">
								La membresía de Bellshipitnow es gratuita, el cliente solo pagará su envío y la tarifa de administración / servicio de $ 35 de los artículos de envío incluidos en una caja con dimensiones máximas de 12x10x9, cualquier caja adicional o cualquier cosa que exceda estas dimensiones no excederá la mitad de la tarifa de administración / servicio.</span>
                                <span class="german" style="display:none;">Die Mitgliedschaft bei Bellshipitnow ist kostenlos. Der Kunde zahlt nur für den Versand und die Verwaltungs- / Servicegebühr von 35 USD für Versandartikel, die in einer Box mit den maximalen Abmessungen 12 x 10 x 9 enthalten sind. Zusätzliche Boxen oder Gegenstände, die diese Abmessungen überschreiten, überschreiten nicht die Hälfte der Verwaltungs- / Servicegebühr.</span>
                                <span class="polish" style="display:none;">
								Członkostwo Bellshipitnow jest bezpłatne, klient zapłaci tylko za wysyłkę i 35 USD opłaty administracyjnej / serwisowej za przesyłki zawarte w pudełku o maksymalnych wymiarach 12 x 10 x 9, wszelkie dodatkowe pudełka lub cokolwiek przekraczającego te wymiary nie przekroczy połowy opłaty administracyjnej / serwisowej.</span>
                                <span class="portuguese" style="display:none;">
								A associação da Bellshipitnow é gratuita, o cliente pagará apenas pelo frete e pela taxa de administração / serviço de US $ 35 dos itens de remessa fechados em uma caixa com dimensões máximas de 12x10x9, quaisquer caixas adicionais ou qualquer coisa que exceda essas dimensões não excederá metade da taxa de administração / serviço.</span>
                                <span class="mandrin" style="display:none;">Bellshipitnow的会员资格是免费的，客户只需为装在最大尺寸为12x10x9的盒子中的所有运输商品和$ 35的管理费/服务费付款，任何其他盒子或超出这些尺寸的任何物品都不会超过管理/服务费的一半。</span>
								****
                        </div>
                    </div>
                </div> <!-- END col-lg-6 -->
                <div class="col-lg-5 ml-auto text-center" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                    data-aos-duration="2000">
                    <img class="wow fadeInRight w-200 rounded" src="assets-theme/img/about/sc-2.jpg"
                        alt="wordwide delivery">
                </div> <!-- END col-lg-6 pl-lg-5 -->
            </div> <!-- END row-->
        </div> <!-- END container-->
    </section>
    <?php require_once("footer.php");?>

    <div class="scroll-top bg-white box-shadow-v1">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <script src="assets-theme/js/bundle.js"></script>
    <script src="assets-theme/js/fury.js"></script>
    <script>
    AOS.init();
    </script>
</body>

</html>