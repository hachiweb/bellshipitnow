<?php
// *************************************************************************
// *                                                                       *
// * DEPRIXA -  Integrated Web system                                      *
// * Copyright (c) JAOMWEB. All Rights Reserved                            *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: osorio2380@yahoo.es                                            *
// * Website: http://www.jaom.info                                         *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.                              *
// * If you Purchased from Codecanyon, Please read the full License from   *
// * here- http://codecanyon.net/licenses/standard                         *
// *                                                                       *
// *************************************************************************

define("_VALID_PHP", true);
require_once("init.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154553863-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-154553863-1');
    </script>

    <!--Meta-->
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="Courier DEPRIXA-Integral Web System" />
    <meta name="author" content="Jaomweb">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!--Favicon-->
    <link rel="icon" href="uploads/favicon.png">

    <!-- Title-->
    <title> Shop US Stores and Ship worldwide | <?php echo $core->site_name;?>Shop US Stores and Ship worldwide</title>

    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700%7COpen+Sans:400,600,700" rel="stylesheet">

    <!-- AOS -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!--Icon fonts-->
    <link rel="stylesheet" href="assets-theme/vendor/strokegap/style.css">
    <link rel="stylesheet" href="assets-theme/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets-theme/vendor/linearicons/style.css">
    <link rel="stylesheet" href="assets-theme/css/bundle.css">
    <link rel="stylesheet" href="assets-theme/css/style.css">
    <link rel="stylesheet" href="dashboard/dist/css/custom.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- <link href="assets/css/custom.css" rel="stylesheet" /> -->
    <script>
    window.projectServices = {
        ASSETS_ENV: "prod",
        brand: "safm",
        version: "stable",
        disableOpacityChange: true
    };
    </script>
    <script type="text/javascript" src="assets.maerskline.com/integrated-global-nav/2/loader.js"></script>

<style> 
#subscribeModal .modal-content{
	overflow:hidden;
}
#subscribeModal .form-control {
    height: 56px;
    border-top-left-radius: 30px;
    border-bottom-left-radius: 30px;
	padding-left:30px;
}
#subscribeModal .btn {
    border-radius: 0px;
    padding: 8px;
    background: #007b5e;
    border-color: #007b5e;
    position: absolute;
    top: 0px;
    right: 0px;
}
#subscribeModal .form-control:focus {
    color: #495057;
    background-color: #fff;
    border-color: #007b5e;
    outline: 0;
    box-shadow: none;
}
#subscribeModal .top-strip{
	height: 155px;
    background: #007b5e;
    transform: rotate(141deg);
    margin-top: -94px;
    margin-right: 190px;
    margin-left: -130px;
    border-bottom: 65px solid #4CAF50;
    border-top: 10px solid #4caf50;
}
#subscribeModal .bottom-strip{
	height: 155px;
    background: #007b5e;
    transform: rotate(112deg);
    margin-top: -110px;
    margin-right: -215px;
    margin-left: 300px;
    border-bottom: 65px solid #4CAF50;
    border-top: 10px solid #4caf50;
}

.modal-body h4{
    color:#004994;
    font-weight: bold;
}
/****** extra *******/
#Reloadpage{
    cursor:pointer;
}
    </style>
</head>

<body id="top">

    <header class="header-top bg-gray-v1 pt-2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 mb-3 mb-md-0 text-center text-md-left">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-3">
                            <span class="icon icon-Phone2 text-primary mr-1"></span> (800) 313-5960
                        </li>
                        <li class="list-inline-item">
                            <span class="icon icon-Mail text-primary mr-1"></span> <a
                                href="mailto:support@jaom.info">Support@Bellshipitnow.com</a>
                        </li>
                    </ul> <!-- END list-inline-->
                </div> <!-- END col-md-6 -->
                <div class="col-md-6 mb-3 mb-md-0">
                    <ul
                        class="list-inline social social-rounded social-default social-sm mb-0 text-center text-md-right">
                        <li class="list-inline-item">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <!-- <li class="list-inline-item">
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </li> -->
                        <li class="list-inline-item">
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div> <!-- END col-md-6 -->
            </div> <!-- END row-->
        </div> <!-- END container-->
    </header> <!-- END header-top-->

    <header class="header header-shrink sticky-top bg-gray-v1">
        <div class="container">
            <nav class="navbar navbar-expand-lg px-md-0">

                <?php require_once("header.php");?>

            </nav>
        </div> <!-- END container-->
    </header> <!-- END header-inverse -->

    <div id="sign-up-alert"
        class="alert alert-success alert-dismissible m-0 rounded-0 custom-alert <?php if($_GET['success']==null){echo 'd-none';}?>">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h1 class="text-center m-0"><strong>Success!</strong></h1>
        <h3 class="text-center m-0">Wellcome to Bellshipitnow, You have signed up successfully.</h3>
    </div>
    <section
        style="background:#7C7C7C url(assets-theme/img/keynote.jpg) no-repeat; background-size:cover; background-position: top right;">
        <div class="overlay bg-black-opacity-0_5"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 m-auto text-center">
                    <h2 class="h1 text-white mb-4" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                        data-aos-duration="2000">
                        HOW BELLSHIPITNOW WORKS
                    </h2>
                    <a data-fancy href="https://www.youtube.com/watch?v=6Ab05QNCK_c" data-fancybox autoplay>
                        <span class="icon icon-Play u-fs-50 u-fs-md-72 text-white"></span>
                    </a>
                </div>
            </div> <!-- END row-->
        </div> <!-- END container-->
    </section> <!-- END section-->
    <section class="u-py-md-250 u-flex-center"
        style="background:#ECF5FE url(assets-theme/img/startup/987888.jpg) no-repeat; background-size:cover; background-position: bottom;min-height:500px;width:100%;margin:auto;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 mb-5 mb-lg-0 mx-auto caption-area px-5">
                    <h1 class="display-4 u-fw-600" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                        data-aos-duration="2000">
                        <span class="font--display-4 clr" style="color:white;">
                            <span class="eng">Your Personal Worldwide</span>
                            <span class="french" style="display:none;">Votre personnel dans le monde</span>
                            <span class="hin" style="display:none;">आपकी निजी दुनिया भर में</span>
                            <span class="arabic" style="display:none;">شخصيتك الشخصية في جميع أنحاء العالم</span>
                            <span class="indonesian" style="display:none;">
                                Seluruh Dunia Pribadi Anda</span>
                            <span class="ban" style="display:none;">আপনার ব্যক্তিগত বিশ্বব্যাপী</span>
                            <span class="italian" style="display:none;">
                                Il tuo personale in tutto il mondo</span>
                            <span class="spanish" style="display:none;">
                                Su personal en todo el mundo</span>
                            <span class="german" style="display:none;">
                                Ihr persönliches weltweit</span>
                            <span class="polish" style="display:none;">
                                Twoje osobiste na całym świecie</span>
                            <span class="portuguese" style="display:none;">
                                Seu Pessoal Mundial</span>
                            <span class="mandrin" style="display:none;">您的个人全球</span>
                            <br>
                            <span class="eng">Shipper!!</span>
                            <span class="french" style="display:none;">Expéditrice!!</span>
                            <span class="portuguese" style="display:none;">
                                Expedidor</span>
                            <span class="hin" style="display:none;">
                                नौभार परेषक</span>
                            <span class="arabic" style="display:none;">الشاحن</span>
                            <span class="indonesian" style="display:none;">
                                Pengirim</span>
                            <span class="ban" style="display:none;">আপনার ব্যক্তিগত বিশ্বব্যাপী</span>
                            <span class="italian" style="display:none;">
                                Shipper</span>
                            <span class="spanish" style="display:none;">
                                Expedidora</span>
                            <span class="german" style="display:none;">
                                Absender</span>
                            <span class="polish" style="display:none;">
                                Spedytor</span>
                            <span class="mandrin" style="display:none;">托运人!!</span>

                        </span> <span class="text-white"></span>
                    </h1>
                    <h4 class="u-fs-22 u-lh-1_8 my-4" style="color:white;">
                        <span class="eng">Shop from any U.S. store to your front door.</span>
                        <span class="french" style="display:none;">Achetez dans n'importe quel magasin américain jusqu'à
                            votre porte d'entrée.</span>
                        <span class="portuguese" style="display:none;">
                            Compre de qualquer loja dos EUA à sua porta da frente.</span>
                        <span class="hin" style="display:none;">
                            किसी भी अमेरिकी दुकान से अपने सामने वाले दरवाजे से खरीदारी करें।</span>
                        <span class="arabic" style="display:none;">تسوق من أي متجر في الولايات المتحدة إلى باب
                            منزلك.</span>
                        <span class="indonesian" style="display:none;">
                            Berbelanja dari sembarang toko AS ke pintu depan Anda.</span>
                        <span class="ban" style="display:none;">যে কোনও মার্কিন স্টোর থেকে আপনার সামনের দরজা পর্যন্ত
                            কেনাকাটা করুন।</span>
                        <span class="italian" style="display:none;">
                            Acquista da qualsiasi negozio degli Stati Uniti alla tua porta di casa.</span>
                        <span class="spanish" style="display:none;">
                            Compre desde cualquier tienda de EE. UU. Hasta su puerta de entrada.</span>
                        <span class="german" style="display:none;">
                            Kaufen Sie in jedem US-Geschäft vor Ihrer Haustür ein.</span>
                        <span class="polish" style="display:none;">
                            Kupuj w dowolnym sklepie w USA do swoich drzwi.</span>
                        <span class="mandrin" style="display:none;">从任何一家美国商店到您的前门购物。</span>
                    </h4>
                    <h4 class="font--display-4 u-fs-22 u-lh-1_8 my-4" style="color:white;">
                        <span class="eng">"
                            Free Membership and No Hidden Fees"</span>
                        <span class="french" style="display:none;">"Adhésion gratuite et sans frais cachés"</span>
                        <span class="portuguese" style="display:none;">
                            "Associação gratuita e sem taxas ocultas"</span>
                        <span class="hin" style="display:none;">
                            "फ्री मेंबरशिप और नो हिडन फीस।"</span>
                        <span class="arabic" style="display:none;">"عضوية مجانية وبدون رسوم خفية"</span>
                        <span class="indonesian" style="display:none;">
                            "Associação gratuita e sem taxas ocultas"</span>
                        <span class="ban" style="display:none;">"নিখরচায় সদস্যতা এবং কোনও লুকানো ফি নেই"</span>
                        <span class="italian" style="display:none;">
                            "Iscrizione gratuita e nessun costo nascosto"</span>
                        <span class="spanish" style="display:none;">
                            "Membresía gratuita y sin cargos ocultos"</span>
                        <span class="german" style="display:none;">
                            "Kostenlose Mitgliedschaft und keine versteckten Gebühren"</span>
                        <span class="polish" style="display:none;">
                            "Bezpłatne członkostwo i brak ukrytych opłat"</span>
                        <span class="mandrin" style="display:none;">"免费会员，无隐藏费用"</span>
                    </h4>
                    <a href="sign-up.php" class="btn btn-rounded btn-custom-head  u-w-170 u-mt-15">
                        Get Started
                    </a>
                </div> <!-- END col-lg-12-->


                <!-- <div class="col-lg-5">
					<div class="card box-shadow-v2 bg-white u-of-hidden text-center">
						<form action="result_track_shipment.php" method="post" class="p-4 p-md-5">
							<div class="input-group u-rounded-50 border u-of-hidden u-mb-20">
								<input type="text" name="order_inv" class="form-control border-0 p-3" placeholder="Tracking Number" required>
							</div>
							<button type="submit" name="submit" class="btn btn-custom btn-rounded">
								<span class="icon icon-Truck text-white"></span>&nbsp;&nbsp; Search Tracking
							</button>
						</form>
					</div>
				</div>  -->
            </div> <!-- END row-->
        </div> <!-- END container-->
    </section> <!-- END intro-hero-->

    <div class="bg-clr-set-1">
        <div class="row">
            <div class="col-sm-9 m-auto">
                <div class="row track-shipment-set">
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 box-setup" data-aos="fade-up"
                        data-aos-duration="3000">
                        <div class="boxes shadow-sm mb-2 bg-white rounded">
                            <h4><img src="assets/img/track.png"
                                    alt="shipping with bellshipitnow on international shipping">
                                <span class="eng">Track shipments</span>
                                <span class="hin" style="display:none;">शिपमेंट ट्रैक करना</span>
                                <span class="arabic" style="display:none;">شحنة المسار</span>
                                <span class="indonesian" style="display:none;">
                                    Lacak Pengiriman</span>
                                <span class="french" style="display:none;">Suivi de livraison</span>
                                <span class="ban" style="display:none;">ট্রাকে পরিবহন করা</span>
                                <span class="italian" style="display:none;">Tracciare la spedizione</span>
                                <span class="spanish" style="display:none;">
                                    Rastrear envio</span>
                                <span class="german" style="display:none;">Sendung verfolgen</span>
                                <span class="polish" style="display:none;">
                                    Śledź przesyłkę</span>
                                <span class="portuguese" style="display:none;">
                                    Rastrear Remessa</span>
                                <span class="mandrin" style="display:none;">追踪货运</span>
                            </h4>
                            <input type="text" class="form mb-4" placeholder="Enter a tracking ID">
                            <button type="submit" class="btn btn-rounded btn-custom u-mt-15">
                                <span class="icon icon-Truck text-white"></span>&nbsp;&nbsp;
                                <span class="eng">Search Tracking</span>
                                <span class="french" style="display:none;">
                                    Suivi de recherche</span>
                                <span class="hin" style="display:none;">ट्रैकिंग खोजें</span>
                                <span class="arabic" style="display:none;">تتبع البحث</span>
                                <span class="indonesian" style="display:none;">
                                    Pelacakan Pencarian</span>
                                <span class="ban" style="display:none;">অনুসন্ধান ট্র্যাকিং</span>
                                <span class="italian" style="display:none;">Monitoraggio della ricerca</span>
                                <span class="spanish" style="display:none;">
                                    Seguimiento de búsqueda</span>
                                <span class="german" style="display:none;">Tracking verfolgen</span>
                                <span class="polish" style="display:none;">
                                    Wyszukaj Śledzenie</span>
                                <span class="portuguese" style="display:none;">
                                    Rastreamento de pesquisa</span>
                                <span class="mandrin" style="display:none;">搜索跟踪</span>
                            </button>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 box-setup" data-aos="fade-down"
                        data-aos-duration="3000">
                        <div class="boxes shadow-sm mb-2 bg-white rounded">
                            <h4><img src="assets/img/map.webp" alt="the US brand you love!">
                                <span class="eng">Contact your local office</span>
                                <span class="french" style="display:none;">
                                    Contactez votre bureau local</span>
                                <span class="hin" style="display:none;">अपने स्थानीय कार्यालय से संपर्क करें</span>
                                <span class="arabic" style="display:none;">اتصل بمكتبك المحلي</span>
                                <span class="indonesian" style="display:none;">
                                    Hubungi kantor setempat Anda</span>
                                <span class="ban" style="display:none;">আপনার স্থানীয় অফিসে যোগাযোগ করুন</span>
                                <span class="italian" style="display:none;">Contatta l'ufficio locale</span>
                                <span class="spanish" style="display:none;">
                                    Póngase en contacto con su oficina local</span>
                                <span class="german" style="display:none;">Wenden Sie sich an Ihr lokales Büro</span>
                                <span class="polish" style="display:none;">
                                    Skontaktuj się z lokalnym biurem</span>
                                <span class="portuguese" style="display:none;">
                                    Entre em contato com o escritório local</span>
                                <span class="mandrin" style="display:none;">与当地办事处联系</span>
                            </h4>
                            <input type="text" class="form mb-4" placeholder="Enter a tracking ID">
                            <button type="submit" class="btn btn-rounded btn-custom  u-w-170 u-mt-15 mt-2">
                                <span><i class="fas fa-search text-white"></i></span>&nbsp;&nbsp;
                                <span class="eng">Find</span>
                                <span class="french" style="display:none;">
                                    Trouver</span>
                                <span class="hin" style="display:none;">खोज</span>
                                <span class="arabic" style="display:none;">تجد</span>
                                <span class="indonesian" style="display:none;">
                                    Temukan</span>
                                <span class="ban" style="display:none;">অনুসন্ধান</span>
                                <span class="italian" style="display:none;">Trova</span>
                                <span class="spanish" style="display:none;">
                                    Encontrar</span>
                                <span class="german" style="display:none;">Finden</span>
                                <span class="polish" style="display:none;">
                                    Odnaleźć</span>
                                <span class="portuguese" style="display:none;">
                                    Encontrar</span>
                                <span class="mandrin" style="display:none;">找</span>
                            </button>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up"
                        data-aos-duration="3000">
                        <div class="boxes box-set shadow-sm mb-2 bg-white rounded">
                            <h4><img src="assets/img/price.png" alt="US E-Commerce">
                                <span class="eng">Find a price</span>
                                <span class="hin" style="display:none;">
                                    एक मूल्य का पता लगाएं</span>
                                <span class="arabic" style="display:none;">ابحث عن السعر</span>
                                <span class="ban" style="display:none;">
                                    একটি দাম সন্ধান করুন</span>
                                <span class="indonesian" style="display:none;">Temukan harga</span>
                                <span class="french" style="display:none;">
                                    Trouvez un prix</span>
                                <span class="italian" style="display:none;">
                                    Iscriviti</span>
                                <span class="spanish" style="display:none;">Encuentra un precio</span>
                                <span class="german" style="display:none;">Finde einen Preis</span>
                                <span class="polish" style="display:none;">
                                    Znajdź cenę</span>
                                <span class="portuguese" style="display:none;">Encontre um preço</span>
                                <span class="mandrin" style="display:none;">寻找价格</span>
                            </h4>
                            <h5>
                                <span class="eng">Get a quote that suits your specific shipping needs</span>
                                <span class="french" style="display:none;">
                                    Obtenez un devis adapté à vos besoins d'expédition spécifiques</span>
                                <span class="hin" style="display:none;">एक बोली प्राप्त करें जो आपकी विशिष्ट शिपिंग
                                    आवश्यकताओं के अनुरूप हो</span>
                                <span class="arabic" style="display:none;">احصل على عرض أسعار يناسب احتياجات الشحن
                                    الخاصة بك</span>
                                <span class="indonesian" style="display:none;">
                                    Dapatkan penawaran yang sesuai dengan kebutuhan pengiriman khusus Anda</span>
                                <span class="ban" style="display:none;">আপনার নির্দিষ্ট শিপিংয়ের প্রয়োজন অনুসারে একটি
                                    উদ্ধৃতি পান</span>
                                <span class="italian" style="display:none;">Richiedi un preventivo adatto alle tue
                                    specifiche esigenze di spedizione</span>
                                <span class="spanish" style="display:none;">
                                    Obtenga un presupuesto que se adapte a sus necesidades de envío específicas</span>
                                <span class="german" style="display:none;">Erhalten Sie ein Angebot, das Ihren
                                    spezifischen Versandanforderungen entspricht</span>
                                <span class="polish" style="display:none;">
                                    Uzyskaj ofertę odpowiadającą Twoim konkretnym potrzebom w zakresie wysyłki</span>
                                <span class="portuguese" style="display:none;">
                                    Obtenha uma cotação que atenda às suas necessidades específicas de remessa</span>
                                <span class="mandrin" style="display:none;">获取适合您特定运输需求的报价</span>
                            </h5>
                            <button type="submit" class="btn btn-rounded btn-custom btn-set box-setup">
                                <span><i class="far fa-money-bill-alt text-white"></i></span>&nbsp;&nbsp;
                                <span class="eng">Find a price</span>
                                <span class="hin" style="display:none;">
                                    एक मूल्य का पता लगाएं</span>
                                <span class="arabic" style="display:none;">ابحث عن السعر</span>
                                <span class="ban" style="display:none;">
                                    একটি দাম সন্ধান করুন</span>
                                <span class="indonesian" style="display:none;">Temukan harga</span>
                                <span class="french" style="display:none;">
                                    Trouvez un prix</span>
                                <span class="italian" style="display:none;">
                                    Iscriviti</span>
                                <span class="spanish" style="display:none;">Encuentra un precio</span>
                                <span class="german" style="display:none;">Finde einen Preis</span>
                                <span class="polish" style="display:none;">
                                    Znajdź cenę</span>
                                <span class="portuguese" style="display:none;">Encontre um preço</span>
                                <span class="mandrin" style="display:none;">寻找价格</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end section -->

    <div class="bg-white">
        <div class="row bg-img">
            <div class="col-sm-9 mx-auto">
                <div class="row">
                    <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 text-center">
                        <div class="heading ">
                            <h3 class="font--display-3" style="color:#004994;" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                                Browse Bellshipitnow network
                            </h3>
                            <div class="rich-text" data-aos="fade-up" data-aos-duration="3000">
                                <span class="eng">Our shipping and logistics network provides regular connections to
                                    <br>over 110
                                    countries and 320 ports helping you connect to key markets <br>around the
                                    globes</span>
                                <span class="french" style="display:none;">
                                    Notre réseau d'expédition et de logistique fournit des liaisons régulières vers<br>
                                    plus de 110 pays et 320 ports pour vous aider à vous connecter aux marchés clés<br>
                                    autour des globes</span>
                                <span class="hin" style="display:none;">हमारा शिपिंग और लॉजिस्टिक्स नेटवर्क नियमित
                                    कनेक्शन प्रदान करता है
                                    <br> 110 से अधिक
                                    देशों और 320 बंदरगाहों की मदद से आप प्रमुख बाजारों से जुड़ते हैं
                                    ग्लोब</span>
                                <span class="arabic" style="display:none;">توفر شبكة الشحن والخدمات اللوجستية لدينا
                                    اتصالات منتظمة بأكثر من 110 دولة و 320 منفذًا مما يساعدك على الاتصال بالأسواق
                                    الرئيسية حول العالم</span>
                                <span class="indonesian" style="display:none;">
                                    Jaringan pengiriman dan logistik kami menyediakan koneksi reguler ke
                                    <br> lebih dari 110
                                    negara dan 320 port membantu Anda terhubung ke pasar utama <br> di sekitar
                                    bola-bola</span>
                                <span class="ban" style="display:none;">আমাদের শিপিং এবং লজিস্টিক নেটওয়ার্কগুলিতে
                                    নিয়মিত সংযোগ সরবরাহ করে
                                    <br> 110 এর বেশি
                                    দেশগুলি এবং 320 বন্দরগুলি আপনাকে আশেপাশে মূল বাজারগুলিতে সংযোগ করতে সহায়তা করে
                                    গ্লোব</span>
                                <span class="italian" style="display:none;">La nostra rete di spedizioni e logistica
                                    fornisce collegamenti regolari con
                                    <br> oltre 110
                                    paesi e 320 porti che ti aiutano a connetterti ai mercati chiave in tutto il
                                    globi</span>
                                <span class="spanish" style="display:none;">
                                    Nuestra red de envíos y logística proporciona conexiones regulares a
                                    <br> más de 110
                                    países y 320 puertos que lo ayudan a conectarse a mercados clave en todo el
                                    globos</span>
                                <span class="german" style="display:none;">Unser Versand- und Logistiknetzwerk bietet
                                    regelmäßige Verbindungen zu
                                    <br> über 110
                                    Länder und 320 Häfen, die Ihnen helfen, Verbindungen zu Schlüsselmärkten rund um die
                                    Welt herzustellen
                                    Globen</span>
                                <span class="polish" style="display:none;">
                                    Nasza sieć wysyłkowa i logistyczna zapewnia regularne połączenia z
                                    <br> ponad 110
                                    kraje i 320 portów, które pomagają łączyć się z kluczowymi rynkami w całym
                                    globusy</span>
                                <span class="portuguese" style="display:none;">
                                    Nossa rede de expedição e logística fornece conexões regulares para
                                    <br> acima de 110
                                    países e 320 portas, ajudando você a se conectar aos principais mercados em todo o
                                    mundo
                                    globos</span>
                                <span class="mandrin" style="display:none;">我们的运输和物流网络提供定期连接
                                    <br>超过110
                                    国家和320个港口可帮助您连接到全球各地的主要市场
                                    地球仪</span>

                            </div>
                            <div class="button-group" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                                data-aos-duration="2000">
                                <a class="btn btn-rounded btn-custom btn-set" href="#">
                                    Browse Bellshipitnow
                                    network

                                </a>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-4" id="counter">
                                <h3 class="font--display-3 counter-value" style="color:#004994;" data-count="2018">

                                </h3>
                                <h5>COMPANY ESTABLISHED</h5>
                            </div>
                            <div class="col-4" id="counter">
                                <h3 class="font--display-3 counter-value" style="color:#004994;" data-count="120">

                                </h3>
                                <h5>COUNTRIES SERVED</h5>
                            </div>
                            <div class="col-4" id="counter">
                                <h3 class="font--display-3 counter-value" style="color:#004994;" data-count="540">

                                </h3>
                                <h5>PORTS SERVED</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 box-setup" data-aos="fade-up"
                        data-aos-duration="3000">
                        <div class="boxes shadow-sm p-3 mb-5 bg-white rounded text-center">
                            <h4 class="font--display-3" style="color:#004994; font-size:22px;">
                                <span class="eng">Find a schedule</span>
                                <span class="hin" style="display:none;">
                                    एक शेड्यूल ढूंढें</span>
                                <span class="arabic" style="display:none;">ابحث عن جدول</span>
                                <span class="ban" style="display:none;">
                                    একটি তফসিল সন্ধান করুন</span>
                                <span class="indonesian" style="display:none;">Temukan jadwal</span>
                                <span class="french" style="display:none;">
                                    Trouvez un horaire</span>
                                <span class="italian" style="display:none;">
                                    Trova un programma</span>
                                <span class="spanish" style="display:none;">Encuentra un horario</span>
                                <span class="german" style="display:none;">Finden Sie einen Zeitplan</span>
                                <span class="polish" style="display:none;">
                                    Znajdź harmonogram</span>
                                <span class="portuguese" style="display:none;">Encontre uma programação</span>
                                <span class="mandrin" style="display:none;">查找时间表</span>
                            </h4>
                            <p>
                                <span class="eng">Search our extensive routes to find the schedule which fits <br>your
                                    supply chain.</span>

                                <span class="french" style="display:none;">
                                    Parcourez nos nombreux itinéraires pour trouver l'horaire qui vous convient<br>
                                    votre chaîne d'approvisionnement.</span>
                                <span class="hin" style="display:none;">शेड्यूल खोजने के लिए हमारे व्यापक मार्गों को
                                    खोजें जो आपके फिट बैठता है
                                    आपूर्ति श्रृंखला।</span>
                                <span class="arabic" style="display:none;">ابحث في طرقنا الواسعة للعثور على الجدول الذي
                                    يناسبك
                                    الموردين.</span>
                                <span class="indonesian" style="display:none;">
                                    Cari rute kami yang luas untuk menemukan jadwal yang sesuai dengan Anda
                                    rantai pasokan.</span>
                                <span class="ban" style="display:none;">আপনার - এর অনুসারে তফসিলটি নির্ধারণ করতে আমাদের
                                    বিস্তৃত রুট অনুসন্ধান করুন
                                    সরবরাহ চেইন</span>
                                <span class="italian" style="display:none;">Cerca i nostri numerosi percorsi per trovare
                                    il programma adatto al tuo
                                    catena di fornitura.</span>
                                <span class="spanish" style="display:none;">
                                    Busque en nuestras extensas rutas para encontrar el horario que se ajuste a su
                                    cadena de suministro.</span>
                                <span class="german" style="display:none;">Durchsuchen Sie unsere umfangreichen Routen,
                                    um den Zeitplan zu finden, der zu Ihnen passt
                                    Lieferkette.</span>
                                <span class="polish" style="display:none;">
                                    Przeszukaj nasze rozległe trasy, aby znaleźć harmonogram, który pasuje do Twojego
                                    łańcuch dostaw.</span>
                                <span class="portuguese" style="display:none;">
                                    Pesquise nossas rotas extensas para encontrar a programação que se encaixa no seu
                                    cadeia de mantimentos.</span>
                                <span class="mandrin" style="display:none;">搜索我们广泛的路线，以找到适合您的时间表<br>
                                    供应链。</span>
                            </p>
                            <input type="text" class="form" placeholder="Origin">
                            <input type="text" class="form" placeholder="Destination">
                            <button type="submit" class="btn btn-rounded btn-custom my-4" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pro-sec">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-8 img-sec mx-auto" data-aos="fade-up"
                    data-aos-duration="3000">
                    <img src="assets/img/s-1.png" alt="think global shipping expert award">
                </div>
            </div>
        </div>
    </div>
    <div class="bg-color-set">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center txt-setup">
                    <h2 class="font--display-3" style="color:white;" data-aos="flip-left"
                        data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                        <span class="eng"> Professional shipping solution for every need</span>
                        <span class="french" style="display:none;">
                        Solution d'expédition professionnelle pour chaque besoin</span>
                                <span class="hin" style="display:none;">हर जरूरत के लिए पेशेवर शिपिंग समाधान</span>
                                <span class="arabic" style="display:none;">حل الشحن المهنية لكل حاجة</span>
                                <span class="indonesian" style="display:none;">
                                Solusi pengiriman profesional untuk setiap kebutuhan</span>
                                <span class="ban" style="display:none;">প্রতিটি প্রয়োজনের জন্য পেশাদার শিপিং সমাধান</span>
                                <span class="italian" style="display:none;">Soluzione di spedizione professionale per ogni esigenza</span>
                                <span class="spanish" style="display:none;">
                                Solución de envío profesional para cada necesidad.</span>
                                <span class="german" style="display:none;">Professionelle Versandlösung für jeden Bedarf</span>
                                <span class="polish" style="display:none;">
                                Profesjonalne rozwiązanie wysyłkowe dla każdej potrzeby</span>
                                <span class="portuguese" style="display:none;">
                                Solução de remessa profissional para todas as necessidades</span>
                                <span class="mandrin" style="display:none;">满足各种需求的专业运输解决方案</span>
                    </h2>
                    <div class="u-h-4 u-w-70 brder-clr rounded mt-4 u-mb-30 mx-auto"></div>
                    <p class="rich-text text-white" data-aos="fade-up" data-aos-duration="3000">
                        <span class="eng">Our mission is to
                            provide customer satisfaction, superlative customer service, maximize customer savings and
                            <br>quick shipping access to our members worldwide. At Bellshipitnow, we treat your package
                            as
                            we would treat our own.</span>
                        <span class="french" style="display:none;">
                            Notre mission est de fournir une satisfaction client, un service client exceptionnel, de
                            maximiser les économies clients et
                            <br>
                            accès rapide à nos membres dans le monde entier. Chez Bellshipitnow, nous traitons votre
                            colis comme nous traiterions le nôtre.</span>
                        <span class="hin" style="display:none;">हमारा मिशन है
                            ग्राहकों की संतुष्टि, शानदार ग्राहक सेवा प्रदान करें, ग्राहक की बचत को अधिकतम करें और
                            दुनिया भर में हमारे सदस्यों के लिए त्वरित शिपिंग एक्सेस। Bellshipitnow में, हम आपके पैकेज का
                            इलाज करते हैं
                            जैसा
                            हम अपना इलाज करेंगे।</span>
                        <span class="arabic" style="display:none;">مهمتنا هي
                            توفير رضا العملاء ، وخدمة العملاء الفائقة ، وزيادة مدخرات العملاء
                            <br> وصول سريع لشحن أعضائنا في جميع أنحاء العالم. في Bellshipitnow ، نتعامل مع مجموعتك
                            مثل
                            سوف نتعامل مع أنفسنا.</span>
                        <span class="indonesian" style="display:none;">
                            Misi kami adalah untuk
                            memberikan kepuasan pelanggan, layanan pelanggan superlatif, memaksimalkan penghematan
                            pelanggan dan
                            <br> akses pengiriman cepat ke anggota kami di seluruh dunia. Di Bellshipitnow, kami
                            memperlakukan paket Anda
                            sebagai
                            kita akan memperlakukan milik kita sendiri.</span>
                        <span class="ban" style="display:none;">আমাদের লক্ষ্য
                            গ্রাহক সন্তুষ্টি, চমত্কার গ্রাহক পরিষেবা প্রদান, গ্রাহক সর্বাধিক সঞ্চয় এবং
                            <br> বিশ্বব্যাপী আমাদের সদস্যদের দ্রুত শিপিংয়ের অ্যাক্সেস। বেলশিপত্নোতে আমরা আপনার
                            প্যাকেজটি ব্যবহার করি
                            যেমন
                            আমরা আমাদের নিজস্ব আচরণ করবে।</span>
                        <span class="italian" style="display:none;">La nostra missione è di
                            fornire soddisfazione del cliente, servizio clienti superlativo, massimizzare i risparmi dei
                            clienti e
                            <br> accesso rapido per la spedizione ai nostri membri in tutto il mondo. In Bellshipitnow
                            trattiamo il tuo pacco
                            come
                            vorremmo trattare il nostro.</span>
                        <span class="spanish" style="display:none;">
                            Nuestra misión es
                            proporcionar satisfacción al cliente, servicio al cliente superlativo, maximizar el ahorro
                            del cliente y
                            Acceso rápido a nuestros miembros en todo el mundo. En Bellshipitnow, tratamos su paquete
                            como
                            trataríamos lo nuestro.</span>
                        <span class="german" style="display:none;">Unsere Mission ist es
                            Kundenzufriedenheit, Kundenservice der Superlative, Maximierung der Kundeneinsparungen und
                            <br> Schneller Versandzugang zu unseren Mitgliedern weltweit. Bei Bellshipitnow behandeln
                            wir Ihr Paket
                            wie
                            wir würden unsere eigenen behandeln.</span>
                        <span class="polish" style="display:none;">
                            Naszą misją jest
                            zapewnić satysfakcję klienta, doskonałą obsługę klienta, zmaksymalizować oszczędności
                            klientów i
                            <br> szybki dostęp do wysyłki do naszych członków na całym świecie. W Bellshipitnow
                            traktujemy twoją paczkę
                            tak jak
                            traktowalibyśmy swoje.</span>
                        <span class="portuguese" style="display:none;">
                            Nossa missão é
                            proporcionar satisfação do cliente, atendimento superlativo ao cliente, maximizar a economia
                            do cliente e
                            <br> acesso rápido aos nossos membros em todo o mundo. No Bellshipitnow, tratamos seu pacote
                            Como
                            nós trataríamos os nossos.</span>
                        <span class="mandrin" style="display:none;">我们的使命是
                            提供客户满意度，最优质的客户服务，最大程度地节省客户并
                            <br>快速运送到我们全球的会员。 在Bellshipitnow，我们会处理您的包裹
                            如
                            我们会自己对待。</span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 img-set">
                    <a href="#" class="">
                        <img src="assets/img/usps.png" alt="superlative customer usps service"></a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 img-set">
                    <a href="#" class="">
                        <img src="assets/img/fedx.png" alt="superlative customer fedx service"></a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 img-set">
                    <a href="#" class="">
                        <img src="assets/img/dhl.jpg" alt="superlative customer dhl service"></a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 img-set">
                    <a href="#" class="">
                        <img src="assets/img/ups.png" alt="superlative customer ups service"> </a>
                </div>
                <div class="col-12">
                    <div class="btn-set text-center">
                        <a href="shipping-rates.php" class="btn btn-rounded btn-custom" data-aos="flip-left"
                            data-aos-easing="ease-out-cubic" data-aos-duration="2000"> GET A QUOTE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php /*<section id="about" class="pb-0">
		<div class="container">
			<div class="row text-center">
				<div class="col-lg-9 mx-auto">
					<h2 class="h1">
						Professional shipping solution for every need
					</h2>
					<div class="u-h-4 u-w-70 bg-primary rounded mt-4 u-mb-30 mx-auto"></div>
					<p>
						Our mission is to provide customer satisfaction, superlative customer service, maximize customer savings and quick shipping access to our members worldwide. At Bellshipitnow, we treat your package as we would treat our own.
					</p>
					<a href="shipping-rates.php" class="btn btn-primary btn-rounded mt-4">GET A QUOTE</a>
				</div>
				<div class="col-12 u-mt-10 text-center">
					<img src="assets-theme/img/startup/s-1.png" alt="">
				</div>
			</div> <!-- END row-->
		</div> <!-- END container-->
	</section> <!-- END section--> */ ?>

    <div class="pro-sec">
        <div class="row">
            <div class="col-9 mx-auto">
                <div class="row">
                    <div class="col-6 my-auto">
                        <div class="">
                            <h2 class="font--display-3" style="color:#004994;" data-aos="flip-right"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">

                                <span class="eng">Choose a Courier</span>
                                <span class="hin" style="display:none;">
                                    एक कूरियर चुनें</span>
                                <span class="arabic" style="display:none;">اختر ساعي</span>
                                <span class="indonesian" style="display:none;">Pilih Kurir</span>
                                <span class="french" style="display:none;">
                                    Choisissez un coursier</span>
                                <span class="ban" style="display:none;">
                                    একটি কুরিয়ার চয়ন করুন</span>
                                <span class="italian" style="display:none;">
                                    Scegli un corriere</span>
                                <span class="spanish" style="display:none;">Elige un servicio de mensajería</span>
                                <span class="german" style="display:none;">Wählen Sie einen Kurier</span>
                                <span class="polish" style="display:none;">
                                    Wybierz kuriera</span>
                                <span class="portuguese" style="display:none;">Escolha um Courier</span>
                                <span class="mandrin" style="display:none;">选择快递</span>
                            </h2>
                            <div class="rich-text">
                                <p>
                                    <span class="eng">That will guarantee a safe and fast shipping to any location. We
                                        also work with
                                        multiple major carriers for the lowest rates to SAVE you money.</span>

                                    <span class="french" style="display:none;">
                                        Cela garantira une expédition sûre et rapide vers n'importe quel endroit. Nous
                                        travaillons également avec plusieurs grands transporteurs pour les tarifs les
                                        plus bas pour vous faire économiser de l'argent.</span>
                                    <span class="hin" style="display:none;">यह किसी भी स्थान के लिए एक सुरक्षित और तेज
                                        शिपिंग की गारंटी देगा। हम
                                        के साथ भी काम करते हैं
                                        आप पैसे बचाने के लिए सबसे कम दरों के लिए कई प्रमुख वाहक।</span>
                                    <span class="arabic" style="display:none;">سيضمن لك الشحن الآمن والسريع إلى أي مكان.
                                        نحن
                                        يعمل أيضا مع
                                        العديد من شركات النقل الرئيسية للحصول على أقل الأسعار لتوفير المال.</span>
                                    <span class="indonesian" style="display:none;">
                                        Itu akan menjamin pengiriman yang aman dan cepat ke lokasi mana pun. Kita
                                        juga bekerja dengan
                                        beberapa operator utama untuk tarif terendah untuk MENGHEMAT uang Anda.</span>
                                    <span class="ban" style="display:none;">এটি যে কোনও স্থানে নিরাপদ এবং দ্রুত
                                        শিপিংয়ের গ্যারান্টি দেবে। আমরা
                                        সাথে কাজ
                                        আপনার অর্থ সাশ্রয় করার জন্য সর্বনিম্ন হারের জন্য একাধিক প্রধান
                                        ক্যারিয়ার।</span>
                                    <span class="italian" style="display:none;">Ciò garantirà una spedizione sicura e
                                        veloce in qualsiasi luogo. Noi
                                        lavorare anche con
                                        più vettori principali per le tariffe più basse per risparmiare denaro.</span>
                                    <span class="spanish" style="display:none;">
                                        Eso garantizará un envío seguro y rápido a cualquier ubicación. Nosotros
                                        también trabajar con
                                        múltiples operadores principales por las tarifas más bajas para AHORRARLE
                                        dinero.</span>
                                    <span class="german" style="display:none;">Dies garantiert einen sicheren und
                                        schnellen Versand an jeden Ort. Wir
                                        auch arbeiten mit
                                        Mehrere große Fluggesellschaften für die niedrigsten Preise, um Geld zu
                                        sparen.</span>
                                    <span class="polish" style="display:none;">
                                        To zagwarantuje bezpieczną i szybką wysyłkę do dowolnego miejsca. My
                                        współpracuję również z
                                        wielu głównych przewoźników dla najniższych stawek, aby zaoszczędzić
                                        pieniądze.</span>
                                    <span class="portuguese" style="display:none;">
                                        Isso garantirá um transporte rápido e seguro para qualquer local. Nós
                                        também trabalha com
                                        várias grandes operadoras pelas tarifas mais baixas para economizar seu
                                        dinheiro.</span>
                                    <span class="mandrin" style="display:none;">这样可以保证安全，快速地运送到任何地方。 我们
                                        也与
                                        多个主要运营商，以最低的价格为您节省金钱。</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6" data-aos="fade-up" data-aos-duration="3000">
                        <div class="img-corner-set">
                            <img src="assets/img/oceantransport_cc.jpg" alt="Ocean Transport" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-5">
        <div class="row">
            <div class="col-9 mx-auto">
                <div class="row">
                    <div class="col-6" data-aos="fade-up" data-aos-duration="3000">
                        <div class="img-corner-set">
                            <img src="assets/img/inland-cargoservices_cc.jpg"
                                alt="quick shipping access to our members worldwide" />
                        </div>
                    </div>
                    <div class="col-6 my-auto">
                        <div class="">
                            <h2 class="font--display-3" style="color:#004994;" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">

                                <span class="eng">Documents</span>
                                <span class="hin" style="display:none;">
                                    दस्तावेज़</span>
                                <span class="arabic" style="display:none;">مستندات</span>
                                <span class="indonesian" style="display:none;">Dokumen</span>
                                <span class="french" style="display:none;">
                                    Les documents</span>
                                <span class="ban" style="display:none;">
                                    কাগজপত্র</span>
                                <span class="italian" style="display:none;">
                                    Documenti</span>
                                <span class="spanish" style="display:none;">Documentos</span>
                                <span class="german" style="display:none;">Unterlagen</span>
                                <span class="polish" style="display:none;">
                                    Dokumenty</span>
                                <span class="portuguese" style="display:none;">Documentos</span>
                                <span class="mandrin" style="display:none;">文件资料</span>
                            </h2>
                            <div class="rich-text">
                                <p>
                                    <span class="eng">Bellshipitnow processes export paperwork for you. Our compliance
                                        knowledge allows us
                                        to safely ship a variety of US products to you that most others cannot.</span>

                                    <span class="french" style="display:none;">
                                        Bellshipitnow traite les documents d'exportation pour vous. Nos connaissances en
                                        matière de conformité nous permettent de vous expédier en toute sécurité une
                                        variété de produits américains que la plupart des autres ne peuvent pas.</span>
                                    <span class="hin" style="display:none;">Bellshipitnow आप के लिए कागजी कार्रवाई का
                                        निर्यात करता है। हमारा अनुपालन
                                        ज्ञान हमें अनुमति देता है
                                        सुरक्षित रूप से आपके लिए विभिन्न अमेरिकी उत्पादों को शिप करना है जो अधिकांश अन्य
                                        नहीं कर सकते हैं।</span>
                                    <span class="arabic" style="display:none;">تقوم Bellshipitnow بمعالجة الأوراق الخاصة
                                        بك. التزامنا
                                        المعرفة تسمح لنا
                                        لشحن مجموعة متنوعة من المنتجات الأمريكية بأمان لا يمكن لمعظم الآخرين القيام
                                        بها.</span>
                                    <span class="indonesian" style="display:none;">
                                        Bellshipitnow memproses dokumen ekspor untuk Anda. Kepatuhan kami
                                        pengetahuan memungkinkan kita
                                        untuk mengirimkan berbagai produk AS dengan aman kepada Anda yang kebanyakan
                                        orang tidak bisa.</span>
                                    <span class="ban" style="display:none;">বেলশিপ্টনো আপনার জন্য রফতানি কাগজপত্রে
                                        প্রসেস করে। আমাদের সম্মতি
                                        জ্ঞান আমাদের অনুমতি দেয়
                                        আপনার কাছে মার্কিন যুক্তরাষ্ট্রের বিভিন্ন পণ্য নিরাপদে জাহাজে পাঠানো যা অন্য
                                        বেশিরভাগ লোকেরা পারে না।</span>
                                    <span class="italian" style="display:none;">Bellshipitnow elabora i documenti per
                                        l'esportazione. La nostra conformità
                                        la conoscenza ci consente
                                        per spedire in sicurezza una varietà di prodotti statunitensi che la maggior
                                        parte degli altri non può.</span>
                                    <span class="spanish" style="display:none;">
                                        Bellshipitnow procesa documentos de exportación para usted. Nuestro cumplimiento
                                        el conocimiento nos permite
                                        para enviarle de manera segura una variedad de productos estadounidenses que la
                                        mayoría de los demás no pueden enviar.</span>
                                    <span class="german" style="display:none;">Bellshipitnow verarbeitet Exportpapiere
                                        für Sie. Unsere Einhaltung
                                        Wissen erlaubt es uns
                                        um eine Vielzahl von US-Produkten sicher an Sie zu versenden, die die meisten
                                        anderen nicht können.</span>
                                    <span class="polish" style="display:none;">
                                        Bellshipitnow przetwarza dla ciebie formalności związane z eksportem. Nasza
                                        zgodność
                                        wiedza pozwala nam
                                        aby bezpiecznie wysyłać do Ciebie różnorodne amerykańskie produkty, których
                                        większość nie może.</span>
                                    <span class="portuguese" style="display:none;">
                                        A Bellshipitnow processa a documentação de exportação para você. Nossa
                                        conformidade
                                        o conhecimento nos permite
                                        enviar com segurança uma variedade de produtos dos EUA para você que a maioria
                                        dos outros não pode.</span>
                                    <span class="mandrin" style="display:none;">Bellshipitnow 为您处理出口文书工作。 我们的合规
                                        知识使我们
                                        安全地运送各种美国产品给您，而大多数其他产品则不能。</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pro-sec">
        <div class="row">
            <div class="col-9 mx-auto">
                <div class="row">
                    <div class="col-6 my-auto">
                        <div class="">
                            <h2 class="font--display-3" style="color:#004994;" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                                Transport
                            </h2>
                            <div class="rich-text">
                                <p>
                                    <span class="eng">All packages received are accurately inspected, then we
                                        consolidate your goods to
                                        SAVE you money and prepare them for timely shipping. We are here to help and
                                        guide
                                        you through the entire process, worry free.</span>
                                    <span class="french" style="display:none;">
                                        Tous les colis reçus sont inspectés avec précision, puis nous consolidons vos
                                        marchandises pour vous faire économiser de l'argent et les préparer pour une
                                        expédition rapide. Nous sommes là pour vous aider et vous guider tout au long du
                                        processus, sans souci.</span>

                                    <span class="hin" style="display:none;">प्राप्त सभी पैकेजों का सटीक निरीक्षण किया
                                        जाता है, फिर हम
                                        करने के लिए अपने माल को मजबूत
                                        आप पैसे बचाएं और उन्हें समय पर शिपिंग के लिए तैयार करें। हम यहाँ हैं मदद करने के
                                        लिए और
                                        मार्गदर्शक
                                        आप पूरी प्रक्रिया के माध्यम से, चिंता मुक्त।</span>
                                    <span class="arabic" style="display:none;">يتم فحص جميع الطرود المستلمة بدقة ، ثم
                                        نحن
                                        دمج البضائع الخاصة بك ل
                                        وفر لك المال واستعدهم للشحن في الوقت المناسب. نحن هنا للمساعدة و
                                        يرشد
                                        لك خلال العملية بأكملها ، لا تقلق.</span>
                                    <span class="indonesian" style="display:none;">
                                        Semua paket yang diterima diperiksa secara akurat, lalu kami
                                        mengkonsolidasikan barang Anda ke
                                        HEMAT Anda uang dan siapkan mereka untuk pengiriman tepat waktu. Kami di sini
                                        untuk membantu dan
                                        panduan
                                        Anda melalui seluruh proses, jangan khawatir.</span>
                                    <span class="ban" style="display:none;">প্রাপ্ত সমস্ত প্যাকেজগুলি সঠিকভাবে পরীক্ষা
                                        করা হয়, তারপরে আমরা
                                        আপনার পণ্য একত্রীকরণ
                                        আপনার অর্থ সাশ্রয় করুন এবং তাদের সময়মতো শিপিংয়ের জন্য প্রস্তুত করুন। আমরা
                                        এখানে সহায়তা করতে এসেছি এবং
                                        নির্দেশিকা
                                        আপনি সম্পূর্ণ প্রক্রিয়া মাধ্যমে, চিন্তা মুক্ত।</span>
                                    <span class="italian" style="display:none;">Tutti i pacchi ricevuti vengono
                                        accuratamente ispezionati, quindi noi
                                        consolidare i tuoi beni a
                                        RISPARMIA i tuoi soldi e preparali per la spedizione puntuale. Siamo qui per
                                        aiutare e
                                        guida
                                        durante l'intero processo, senza preoccupazioni.</span>
                                    <span class="spanish" style="display:none;">
                                        Todos los paquetes recibidos son inspeccionados con precisión, luego nosotros
                                        consolida tus bienes a
                                        AHORRE dinero y prepárelos para el envío oportuno. Estamos aquí para ayudar y
                                        guía
                                        a través de todo el proceso, sin preocupaciones.</span>
                                    <span class="german" style="display:none;">Alle eingegangenen Pakete werden genau
                                        geprüft, dann wir
                                        konsolidieren Sie Ihre Waren zu
                                        Sparen Sie Geld und bereiten Sie sie auf den rechtzeitigen Versand vor. Wir sind
                                        hier um zu helfen und
                                        leiten
                                        Sie durch den gesamten Prozess sorgenfrei.</span>
                                    <span class="polish" style="display:none;">
                                        Wszystkie otrzymane paczki są dokładnie sprawdzane, a następnie my
                                        skonsoliduj swoje towary do
                                        Zaoszczędź swoje pieniądze i przygotuj je na czas. Jesteśmy tutaj, aby pomóc i
                                        przewodnik
                                        przez cały proces, bez obaw.</span>
                                    <span class="portuguese" style="display:none;">
                                        Todos os pacotes recebidos são inspecionados com precisão, então nós
                                        consolide seus bens para
                                        Economize seu dinheiro e prepare-os para o envio oportuno. Estamos aqui para
                                        ajudar e
                                        guia
                                        você durante todo o processo, sem preocupações.</span>
                                    <span class="mandrin" style="display:none;">所有收到的包裹都经过了准确的检查，然后我们
                                        将您的货物合并到
                                        为您省钱并为及时运输做好准备。 我们在这里为您提供帮助
                                        指南
                                        您在整个过程中，无后顾之忧。</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="img-corner-set" data-aos="fade-up" data-aos-duration="3000">
                            <img src="assets/img/bsn_diverse_group_no_bg.png" alt="wordwide delivery" class="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-5">
        <div class="row">
            <div class="col-9 mx-auto">
                <div class="row">
                    <div class="col-6" data-aos="fade-up" data-aos-duration="3000">
                        <div class="img-corner-set">
                            <img src="assets/img/hero_medium_1300x650_ship---white7acc.jpg" alt="how to ship"
                                class="" />
                        </div>
                    </div>
                    <div class="col-6 my-auto">
                        <div class="">
                            <h2 class="font--display-3" style="color:#004994;" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">

                                <span class="eng">Track</span>
                                <span class="hin" style="display:none;">
                                    धावन पथ</span>
                                <span class="arabic" style="display:none;">المسار</span>
                                <span class="indonesian" style="display:none;">Jalur</span>
                                <span class="french" style="display:none;">
                                    Piste</span>
                                <span class="ban" style="display:none;">
                                    পথ</span>
                                <span class="italian" style="display:none;">
                                    Traccia</span>
                                <span class="spanish" style="display:none;">Pista</span>
                                <span class="german" style="display:none;">Spur</span>
                                <span class="polish" style="display:none;">
                                    Tor</span>
                                <span class="portuguese" style="display:none;">Track</span>
                                <span class="mandrin" style="display:none;">跟踪</span>
                            </h2>
                            <div class="rich-text">
                                <p>
                                    <span class="eng">We assign a unique tracking number that will allow you to check
                                        the status of your
                                        package in real-time.</span>
                                    <span class="french" style="display:none;">
                                        Nous attribuons un numéro de suivi unique qui vous permettra de vérifier l'état
                                        de votre colis en temps réel.</span>

                                    <span class="hin" style="display:none;">हम एक अद्वितीय ट्रैकिंग नंबर प्रदान करते हैं
                                        जो आपको जांचने की अनुमति देगा
                                        आप की स्थिति
                                        वास्तविक समय में पैकेज।</span>
                                    <span class="arabic" style="display:none;">نقوم بتعيين رقم تتبع فريد يسمح لك بالتحقق
                                        وضع الخاص بك
                                        حزمة في الوقت الحقيقي.</span>
                                    <span class="indonesian" style="display:none;">
                                        Kami menetapkan nomor pelacakan unik yang akan memungkinkan Anda untuk memeriksa
                                        status Anda
                                        paket secara real-time.</span>
                                    <span class="ban" style="display:none;">আমরা একটি অনন্য ট্র্যাকিং নম্বর বরাদ্দ করি
                                        যা আপনাকে চেক করতে দেয়
                                        আপনার অবস্থা
                                        রিয়েল-টাইমে প্যাকেজ।</span>
                                    <span class="italian" style="display:none;">Assegniamo un numero di tracciamento
                                        univoco che ti permetterà di verificare
                                        lo stato del tuo
                                        pacchetto in tempo reale.</span>
                                    <span class="spanish" style="display:none;">
                                        Asignamos un número de seguimiento único que le permitirá verificar
                                        el estado de su
                                        paquete en tiempo real.</span>
                                    <span class="german" style="display:none;">Wir weisen Ihnen eine eindeutige
                                        Tracking-Nummer zu, mit der Sie diese überprüfen können
                                        den Status Ihres
                                        Paket in Echtzeit.</span>
                                    <span class="polish" style="display:none;">
                                        Przypisujemy unikalny numer śledzenia, który pozwoli Ci sprawdzić
                                        status twojego
                                        pakiet w czasie rzeczywistym.</span>
                                    <span class="portuguese" style="display:none;">
                                        Atribuímos um número de rastreamento exclusivo que permitirá que você verifique
                                        o status do seu
                                        pacote em tempo real.</span>
                                    <span class="mandrin" style="display:none;">我们分配一个唯一的跟踪号，以便您检查
                                        您的状态
                                        实时打包。</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pro-sec">
        <div class="row">
            <div class="col-9 mx-auto">
                <div class="row">
                    <div class="col-6 my-auto">
                        <div class="">
                            <h2 class="font--display-3" style="color:#004994;" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                                <span class="eng">Shipping Consultation</span>
                                <span class="hin" style="display:none;">
                                    शिपिंग परामर्श</span>
                                <span class="arabic" style="display:none;">استشارة الشحن</span>
                                <span class="indonesian" style="display:none;">Konsultasi Pengiriman</span>
                                <span class="french" style="display:none;">
                                    Consultation d'expédition</span>
                                <span class="ban" style="display:none;">
                                    শিপিং পরামর্শ</span>
                                <span class="italian" style="display:none;">
                                    Consulenza sulla spedizione</span>
                                <span class="spanish" style="display:none;">Consulta de envío</span>
                                <span class="german" style="display:none;">Versandberatung</span>
                                <span class="polish" style="display:none;">
                                    Konsultacja wysyłkowa</span>
                                <span class="portuguese" style="display:none;">Consulta de Remessa</span>
                                <span class="mandrin" style="display:none;">货运咨询</span>
                            </h2>
                            <div class="rich-text">
                                <p>
                                    <span class="eng">We will assist you in determining custom shipping options.</span>
                                    <span class="french" style="display:none;">
                                        Nous vous aiderons à déterminer les options d'expédition personnalisées.</span>

                                    <span class="hin" style="display:none;">हम कस्टम शिपिंग विकल्पों के निर्धारण में
                                        आपकी सहायता करेंगे।</span>
                                    <span class="arabic" style="display:none;">سنساعدك في تحديد خيارات الشحن
                                        المخصصة..</span>
                                    <span class="indonesian" style="display:none;">
                                        Kami akan membantu Anda dalam menentukan opsi pengiriman khusus.</span>
                                    <span class="ban" style="display:none;">আমরা আপনাকে কাস্টম শিপিংয়ের বিকল্পগুলি
                                        নির্ধারণে সহায়তা করব।</span>
                                    <span class="italian" style="display:none;">Ti aiuteremo a determinare le opzioni di
                                        spedizione personalizzate.</span>
                                    <span class="spanish" style="display:none;">
                                        Le ayudaremos a determinar las opciones de envío personalizadas.</span>
                                    <span class="german" style="display:none;">Wir weisen Ihnen eine eindeutige
                                        Wir unterstützen Sie bei der Bestimmung der benutzerdefinierten
                                        Versandoptionen.</span>
                                    <span class="polish" style="display:none;">
                                        Pomożemy Ci w określeniu niestandardowych opcji wysyłki.</span>
                                    <span class="portuguese" style="display:none;">
                                        Ajudaremos você a determinar as opções de envio personalizadas.</span>
                                    <span class="mandrin" style="display:none;">我们将协助您确定定制运输选项。</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6" data-aos="fade-up" data-aos-duration="3000">
                        <div class="img-corner-set">
                            <img src="assets/img/chuttersnap.jpg" alt="Shipping Consultation" class="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-5">
        <div class="row">
            <div class="col-9 mx-auto">
                <div class="row">
                    <div class="col-6" data-aos="fade-up" data-aos-duration="3000">
                        <div class="img-corner-set">
                            <img src="assets/img/digitalsolution_cc.jpg"
                                alt="Superior Customer Support with one of our shipping agents" class="" />
                        </div>
                    </div>
                    <div class="col-6 my-auto">
                        <div class="">
                            <h2 class="font--display-3" style="color:#004994;" data-aos="flip-left"
                                data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                                Superior Customer Support
                                <span class="eng">Shipping Consultation</span>
                                <span class="hin" style="display:none;">
                                    शिपिंग परामर्श</span>
                                <span class="arabic" style="display:none;">استشارة الشحن</span>
                                <span class="indonesian" style="display:none;">Konsultasi Pengiriman</span>
                                <span class="french" style="display:none;">
                                    Consultation d'expédition</span>
                                <span class="ban" style="display:none;">
                                    শিপিং পরামর্শ</span>
                                <span class="italian" style="display:none;">
                                    Consulenza sulla spedizione</span>
                                <span class="spanish" style="display:none;">Consulta de envío</span>
                                <span class="german" style="display:none;">Versandberatung</span>
                                <span class="polish" style="display:none;">
                                    Konsultacja wysyłkowa</span>
                                <span class="portuguese" style="display:none;">Suporte ao Cliente Superior</span>
                                <span class="mandrin" style="display:none;">货运咨询</span>
                            </h2>
                            <div class="rich-text">
                                <p>
                                    <span class="eng">We provide exemplary live customer support with one of our
                                        shipping agents.</span>

                                    <span class="french" style="display:none;">
                                        Nous fournissons un support client en direct exemplaire avec l'un de nos agents
                                        maritimes.</span>
                                    <span class="hin" style="display:none;">हम अपने में से एक के साथ अनुकरणीय लाइव
                                        ग्राहक सहायता प्रदान करते हैं
                                        शिपिंग एजेंट्स।</span>
                                    <span class="arabic" style="display:none;">نحن نقدم دعم العملاء الحي المثالي مع أحد
                                        عملائنا
                                        وكلاء الشحن.</span>
                                    <span class="indonesian" style="display:none;">
                                        Kami memberikan dukungan pelanggan langsung yang patut dicontoh dengan salah
                                        satu dari kami
                                        agen pengiriman.</span>
                                    <span class="ban" style="display:none;">আমরা আমাদের একজনের সাথে অনুকরণীয় লাইভ
                                        গ্রাহক সহায়তা সরবরাহ করি
                                        শিপিং এজেন্ট।</span>
                                    <span class="italian" style="display:none;">Forniamo assistenza clienti dal vivo
                                        esemplare con uno dei nostri
                                        agenti di spedizione.</span>
                                    <span class="spanish" style="display:none;">
                                        Brindamos un servicio al cliente en vivo ejemplar con uno de nuestros
                                        Agentes de envío..</span>
                                    <span class="german" style="display:none;">Wir weisen Ihnen eine eindeutige
                                        Wir bieten beispielhaften Live-Kundensupport mit einem unserer Mitarbeiter
                                        Spediteure.</span>
                                    <span class="polish" style="display:none;">
                                        Zapewniamy wzorową obsługę klienta na żywo w jednym z naszych
                                        Agentów żeglugowych.</span>
                                    <span class="portuguese" style="display:none;">
                                        Fornecemos suporte ao cliente ao vivo exemplar com um de nossos
                                        agentes de transporte.</span>
                                    <span class="mandrin" style="display:none;">我们通过以下方式之一提供出色的实时客户支持：
                                        货运代理。</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- vedio sec -->

    <section class="mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <div class="row shadow bg-white rounded p-4" data-aos="fade-up" data-aos-duration="3000">
                        <div class="col-12">
                            <div class="text-center feature">
                                <h4 class="font--display-3" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                                    data-aos-duration="2000">Features You Will Love</h4>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class=" feature-2">
                                <h6><i class="fas fa-arrow-circle-right"></i><span>Access to thousands of US
                                        stores</span></h6>
                                <h6><i class="fas fa-arrow-circle-right"></i><span>US sales tax-free shopping</span>
                                </h6>
                                <h6><i class="fas fa-arrow-circle-right"></i><span>Discounted shipping rates</span></h6>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class=" feature-2">
                                <h6><i class="fas fa-arrow-circle-right"></i><span>Superior packing & top rated customer
                                        care</span></h6>
                                <h6><i class="fas fa-arrow-circle-right"></i><span>Exclusive savings/coupons</span></h6>
                                <h6><i class="fas fa-arrow-circle-right"></i><span>Package Consolidation* saves you
                                        money</span></h6>
                            </div>
                        </div>
                        <div class="col-12 p-3">
                            <div class="text-center feature">
                                <a href="benefit.php" class="btn btn-rounded btn-custom"> See All Benefits </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- END container-->
    </section>
    <section class="u-py-100 bg-white-v2 pro-sec">
        <div class="container">
            <div class="row text-center">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 box-setup mb-3">
                    <div class="boxes shadow-sm bg-white rounded" data-aos="fade-up" data-aos-duration="3000">
                        <h2 class="font--display-3 my-4" style="color:#004994;">
                            <span class="eng">QUALITY</span>
                            <span class="hin" style="display:none;">
                                गुणवत्ता</span>
                            <span class="arabic" style="display:none;">جودة</span>
                            <span class="indonesian" style="display:none;">KUALITAS</span>
                            <span class="french" style="display:none;">
                                QUALITÉ</span>
                            <span class="ban" style="display:none;">
                                গুণমান</span>
                            <span class="italian" style="display:none;">
                                QUALITÀ</span>
                            <span class="spanish" style="display:none;">CALIDAD</span>
                            <span class="german" style="display:none;">QUALITÄT</span>
                            <span class="polish" style="display:none;">
                                JAKOŚĆ</span>
                            <span class="portuguese" style="display:none;">QUALIDADE</span>
                            <span class="mandrin" style="display:none;">质量</span>
                        </h2>
                        <p class="mb-3">
                            <span class="eng"> We set high standards to deliver an outstanding shipping
                                <br>experience.</span>
                            <span class="hin" style="display:none;">
                                हम एक उत्कृष्ट शिपिंग <br> अनुभव देने के लिए उच्च मानक निर्धारित करते हैं।</span>
                            <span class="arabic" style="display:none;">لقد وضعنا معايير عالية لتقديم تجربة شحن
                                متميزة.</span>
                            <span class="indonesian" style="display:none;">Kami menetapkan standar tinggi untuk
                                memberikan pengalaman pengiriman yang luar biasa.</span>
                            <span class="french" style="display:none;">
                                Nous établissons des normes élevées pour offrir une expérience d'expédition <br>
                                exceptionnelle.</span>
                            <span class="ban" style="display:none;">
                                অসামান্য শিপিংয়ের অভিজ্ঞতা সরবরাহের জন্য আমরা উচ্চ মান নির্ধারণ করেছি।</span>
                            <span class="italian" style="display:none;">
                                Stabiliamo standard elevati per offrire un'esperienza di spedizione eccezionale.</span>
                            <span class="spanish" style="display:none;">Establecemos altos estándares para ofrecer una
                                excelente experiencia de envío.</span>
                            <span class="german" style="display:none;">Wir setzen hohe Standards, um ein hervorragendes
                                Versanderlebnis zu bieten.</span>
                            <span class="polish" style="display:none;">
                                Wyznaczamy wysokie standardy, aby zapewnić wyjątkową jakość wysyłek.</span>
                            <span class="portuguese" style="display:none;">Estabelecemos altos padrões para proporcionar
                                uma excelente experiência de remessa e entrega.</span>
                            <span class="mandrin" style="display:none;">我们设定高标准以提供出色的运输
                                <br>经验。</span>
                        </p>
                        <a href="shipping-rates.php" class="btn btn-rounded btn-custom"> GET A QUOTE</a>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 box-setup mb-3">
                    <div class="boxes shadow-sm bg-white rounded" data-aos="fade-up" data-aos-duration="3000">
                        <h2 class="font--display-3 my-4" style="color:#004994;">
                            <span class="eng">FAST</span>
                            <span class="hin" style="display:none;">
                                तेज</span>
                            <span class="arabic" style="display:none;">بسرعة</span>
                            <span class="indonesian" style="display:none;">CEPAT</span>
                            <span class="french" style="display:none;">
                                VITE</span>
                            <span class="ban" style="display:none;">
                                দ্রুত</span>
                            <span class="italian" style="display:none;">
                                VELOCE</span>
                            <span class="spanish" style="display:none;">RÁPIDA</span>
                            <span class="german" style="display:none;">SCHNELL</span>
                            <span class="polish" style="display:none;">
                                SZYBKI</span>
                            <span class="portuguese" style="display:none;">VELOZES</span>
                            <span class="mandrin" style="display:none;">快速</span>
                        </h2>
                        <p>
                            <span class="eng">You can trust our speedy shipping service, because we know when every
                                second counts.</span>
                            <span class="hin" style="display:none;">
                                आप हमारी त्वरित शिपिंग सेवा पर भरोसा कर सकते हैं, क्योंकि हम जानते हैं कि जब हर दूसरा
                                मायने रखता है।</span>
                            <span class="arabic" style="display:none;">يمكنك الوثوق بخدمة الشحن السريع لدينا ، لأننا
                                نعرف متى يحسب كل ثانية.</span>
                            <span class="indonesian" style="display:none;">Anda dapat mempercayai layanan pengiriman
                                cepat kami, karena kami tahu kapan setiap detik diperhitungkan.</span>
                            <span class="french" style="display:none;">
                                Vous pouvez faire confiance à notre service d'expédition rapide, car nous savons quand
                                chaque seconde compte.</span>
                            <span class="ban" style="display:none;">
                                দ্রুত</span>
                            <span class="italian" style="display:none;">
                                Puoi fidarti del nostro servizio di spedizione veloce, perché sappiamo quando ogni
                                secondo conta.</span>
                            <span class="spanish" style="display:none;">Puede confiar en nuestro servicio de envío
                                rápido, porque sabemos cuándo cada segundo cuenta.</span>
                            <span class="german" style="display:none;">Sie können unserem schnellen Versandservice
                                vertrauen, denn wir wissen, wann jede Sekunde zählt.</span>
                            <span class="polish" style="display:none;">
                                Możesz zaufać naszej usłudze szybkiej wysyłki, ponieważ wiemy, kiedy liczy się każda
                                sekunda.</span>
                            <span class="portuguese" style="display:none;">Você pode confiar em nosso serviço de remessa
                                rápida, porque sabemos quando cada segundo conta.</span>
                            <span class="mandrin" style="display:none;">您可以相信我们迅速的运输服务，因为我们知道每时每刻都至关重要。</span>
                        </p>
                        <a href="shipping-rates.php" class="btn btn-rounded btn-custom"> GET A QUOTE</a>
                    </div>
                </div> <!-- END col-md-4 -->

                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 box-setup mb-3">
                    <div class="boxes shadow-sm bg-white rounded" data-aos="fade-up" data-aos-duration="3000">
                        <h2 class="font--display-3 my-4" style="color:#004994;">
                            <span class="eng">SAFE</span>
                            <span class="hin" style="display:none;">
                                सुरक्षित</span>
                            <span class="arabic" style="display:none;">آمنة</span>
                            <span class="indonesian" style="display:none;">AMAN</span>
                            <span class="french" style="display:none;">
                                SÛRE</span>
                            <span class="ban" style="display:none;">
                                নিরাপদ</span>
                            <span class="italian" style="display:none;">
                                SICURA</span>
                            <span class="spanish" style="display:none;">SEGURA</span>
                            <span class="german" style="display:none;">SICHER</span>
                            <span class="polish" style="display:none;">
                                BEZPIECZNY</span>
                            <span class="portuguese" style="display:none;">SEGURA</span>
                            <span class="mandrin" style="display:none;">安全</span>
                        </h2>
                        <p>
                            <span class="eng">We monitor shipping status and communicate with customers and carriers to
                                provide peace of
                                mind.</span>
                            <span class="hin" style="display:none;">
                                हम शिपिंग की स्थिति की निगरानी करते हैं और ग्राहकों और वाहक के साथ संवाद करते हैं ताकि
                                शांति प्रदान की जा सके
                                मन।</span>
                            <span class="arabic" style="display:none;">نحن نراقب حالة الشحن ونتواصل مع العملاء وشركات
                                النقل لتوفير راحة
                                عقل.</span>
                            <span class="indonesian" style="display:none;">Kami memantau status pengiriman dan
                                berkomunikasi dengan pelanggan dan operator untuk memberikan ketenangan
                                pikiran.</span>
                            <span class="french" style="display:none;">
                                Nous surveillons le statut d'expédition et communiquons avec les clients et les
                                transporteurs pour assurer la tranquillité de
                                esprit.</span>
                            <span class="ban" style="display:none;">
                                আমরা শিপিংয়ের স্থিতি পর্যবেক্ষণ করি এবং গ্রাহক এবং বাহকদের সাথে শান্তি সরবরাহের জন্য
                                যোগাযোগ করি
                                মন।</span>
                            <span class="italian" style="display:none;">
                                Monitoriamo lo stato della spedizione e comunichiamo con clienti e corrieri per
                                garantire tranquillità
                                mente.</span>
                            <span class="spanish" style="display:none;">Monitoreamos el estado del envío y nos
                                comunicamos con los clientes y transportistas para brindarle tranquilidad.
                                mente.</span>
                            <span class="german" style="display:none;">Wir überwachen den Versandstatus und
                                kommunizieren mit Kunden und Spediteuren, um die Sicherheit zu gewährleisten
                                Verstand.</span>
                            <span class="polish" style="display:none;">
                                Monitorujemy status wysyłki i komunikujemy się z klientami i przewoźnikami, aby zapewnić
                                spokój
                                umysł.</span>
                            <span class="portuguese" style="display:none;">Monitoramos o status da remessa e nos
                                comunicamos com clientes e transportadoras para proporcionar paz de
                                mente.</span>
                            <span class="mandrin" style="display:none;">我们监控运输状态并与客户和承运人进行沟通，以确保
                                心神。</span>

                        </p>
                        <a href="shipping-rates.php" class="btn btn-rounded btn-custom"> GET A QUOTE</a>
                    </div>
                </div>
            </div><!-- END row-->
        </div> <!-- END container-->
    </section>


    <section class="u-py-100 bg-white-v2">
        <div class="container">
            <div class="row">
                <div class="col-12 d-md-flex justify-content-between text-center">
                    <div class="p-3">
                        <a href="https://www.amazon.com/"><img src="assets-theme/img/partner/Layer-0.png"
                                alt="amazon"></a>
                    </div>
                    <div class="p-3">
                        <a href="https://www.bebe.com/"><img src="assets-theme/img/partner/Layer-1.png" alt="babe"></a>
                    </div>
                    <div class="p-3">
                        <a href="https://www.express.com/"><img src="assets-theme/img/partner/Layer-2.png"
                                alt="express"></a>
                    </div>
                    <div class="p-3">
                        <a href="https://www.gucci.com/int/en/"><img src="assets-theme/img/partner/Layer-3.png"
                                alt="gucci"></a>
                    </div>
                    <div class="p-3">
                        <a href="https://www.ebay.com/"><img src="assets-theme/img/partner/Layer-4.png" alt="ebay"></a>
                    </div>
                </div>
            </div> <!-- END row-->
        </div> <!-- END container-->
    </section>
<!-- Popup Modal -->
<div class="modal fade text-center py-5"  id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
				<div class="top-strip"></div>
                <!-- <a class="h2" href="https://www.fiverr.com/share/qb8D02" target="_blank">BELLSHIPITNOW</a> -->
                <h4 class="pb-5 mb-0 text-uppercase">Highlights</h4>
                <!-- <p class="pb-1 text-muted"><small>Sign up to update with our latest news and products.</small></p> -->
                    <div class="feature-2 mb-3 mx-5 text-left">
                        <h6 class="m-0"><i class="fas fa-arrow-circle-right"></i><span>Free membership and No hidden fees</span></h6>
                        <!-- <h6 class="m-0"><i class="fas fa-arrow-circle-right"></i><span>Remove Google plus</span></h6> -->
                        <h6 class="m-0"><i class="fas fa-arrow-circle-right"></i><span>Free packager handling</span></h6>
                        <h6 class="m-0"><i class="fas fa-arrow-circle-right"></i><span>Free repacking</span></h6>
                        <h6 class="m-0"><i class="fas fa-arrow-circle-right"></i><span>Free consolidation</span></h6>
    				</div>
                <p class="pb-1 text-muted"><small>Shop from any U.S. store to your front door.</small></p>
                <div class="bottom-strip"></div>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- end Popup Modal -->

    <?php require_once("footer.php");?>

    <div class="scroll-top bg-white box-shadow-v1">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script>
    var a = 0;
    $(window).scroll(function() {

        var oTop = $('#counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
            $('.counter-value').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');
                $({
                    countNum: $this.text()
                }).animate({
                        countNum: countTo
                    },

                    {

                        duration: 3000,
                        easing: 'swing',
                        step: function() {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function() {
                            $this.text(this.countNum);
                            //alert('finished');
                        }

                    });
            });
            a = 1;
        }

    });
    </script>
    <script src="assets-theme/js/bundle.js"></script>
    <script src="assets-theme/js/fury.js"></script>
    <!-- <script src="assets/vendor/parallax.js/parallax.min.js"></script> -->
    <!-- aos -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- <script src="Assets_sc9/safmarine/scripts/main453b.js?v=1.0.7223.26380"></script> -->
    <script>
    AOS.init();
    </script>
    <script>
    $(window).on('load', function() {
	 setTimeout(function(){
	   $('#subscribeModal').modal('show');
    }, 3000);
    setTimeout(function(){
        $('.btn-danger').click();
    }, 15000);
    });
</script>
</body>

</html>