<?php
// *************************************************************************
// *                                                                       *
// * DEPRIXA -  Integrated Web system                                      *
// * Copyright (c) JAOMWEB. All Rights Reserved                            *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: osorio2380@yahoo.es                                            *
// * Website: http://www.jaom.info                                         *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.                              *
// * If you Purchased from Codecanyon, Please read the full License from   *
// * here- http://codecanyon.net/licenses/standard                         *
// *                                                                       *
// *************************************************************************

  define("_VALID_PHP", true);
  require_once("init.php");
  

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!--Meta-->
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="Courier DEPRIXA-Integral Web System" />
    <meta name="author" content="Jaomweb">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--Favicon-->
    <link rel="icon" href="uploads/favicon.png">

    <!-- Title-->
    <title>Contact US <?php echo $core->site_name;?></title>

    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700%7COpen+Sans:400,600,700" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <!--Icon fonts-->
    <link rel="stylesheet" href="assets-theme/vendor/strokegap/style.css">
    <link rel="stylesheet" href="assets-theme/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets-theme/vendor/linearicons/style.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets-theme/css/bundle.css">
    <link rel="stylesheet" href="assets-theme/css/style.css">

    <script>
    window.projectServices = {
        ASSETS_ENV: "prod",
        brand: "safm",
        version: "stable",
        disableOpacityChange: true
    };
    </script>
    <script type="text/javascript" src="assets.maerskline.com/integrated-global-nav/2/loader.js"></script>
</head>

<body id="top">

    <!--headers-->
    <header class="header header-shrink header-inverse fixed-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg px-md-0">

                <?php require_once("header.php");?>

            </nav>
        </div> <!-- END container-->
    </header> <!-- END header -->

    <section data-dark-overlay="6" data-init="parallax" class="u-py-30 u-pt-lg-200 u-pb-lg-50 u-flex-center"
        style=" background:url(assets-theme/img/startup/contact.jpg) no-repeat; background-size:cover; background-position: center;">

        <div class="container">
            <div class="row">
                <div class="col-12 text-center text-white">
                    <h1 class="text-white">
                        <span class="eng">CONTACT US</span>
                        <span class="french" style="display:none;">
						NOUS CONTACTER</span>
                        <span class="hin" style="display:none;">संपर्क करें</span>
                        <span class="arabic" style="display:none;">اتصل بنا</span>
                        <span class="indonesian" style="display:none;">
						HUBUNGI KAMI</span>
                        <span class="ban" style="display:none;">যোগাযোগ করুন</span>
                        <span class="italian" style="display:none;">CONTATTACI</span>
                        <span class="spanish" style="display:none;">
						CONTÁCTENOS</span>
                        <span class="german" style="display:none;">KONTAKTIERE UNS</span>
                        <span class="polish" style="display:none;">
						SKONTAKTUJ SIĘ Z NAMI</span>
                        <span class="portuguese" style="display:none;">
						CONTATE-NOS</span>
                        <span class="mandrin" style="display:none;">联系我们</span>
                    </h1>
                    <div class="u-h-4 u-w-50 bg-white rounded mx-auto my-4"></div>
                </div>
            </div> <!-- END row-->
        </div> <!-- END container-->
    </section> <!-- END intro-hero-->

    <section class="contact-sec-img">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mx-auto text-center">
                    <h2 class="h1 font--display-3" style="color:#004994;font-size:40px;">
                        <span class="eng">We Love To Hear From You</span>
                        <span class="french" style="display:none;">
                            Nous aimons vous entendre</span>
                        <span class="hin" style="display:none;">हमें आपके उत्तर की प्रतीक्षा रहती है</span>
                        <span class="arabic" style="display:none;">نحن نحب أن نسمع منك</span>
                        <span class="indonesian" style="display:none;">
                            Kami Senang Mendengar Dari Anda</span>
                        <span class="ban" style="display:none;">আমরা আপনার কাছ থেকে শুনতে ভালোবাসি</span>
                        <span class="italian" style="display:none;">Adoriamo sentirti</span>
                        <span class="spanish" style="display:none;">
                            Nos encanta saber de ti</span>
                        <span class="german" style="display:none;">Wir lieben es, von Ihnen zu hören</span>
                        <span class="polish" style="display:none;">
                            Kochamy Cię słyszeć</span>
                        <span class="portuguese" style="display:none;">
                            Nós gostamos de ouvir de você</span>
                        <span class="mandrin" style="display:none;">We Love To Hear From You</span>
                    </h2>
                    <div class="u-h-4 u-w-50 rounded mt-4 u-mb-40 mx-auto" style="background-color:#004994"></div>
                    <p class="text-white">
                        <span class="eng">You may contact us with any issue or concern regarding our services. We'll get
                            in touch with you as soon as possible.</span>
                        <span class="french" style="display:none;">
                            Vous pouvez nous contacter pour tout problème ou préoccupation concernant nos services. Nous
                            prendrons contact avec vous dans les plus brefs délais.</span>
                        <span class="hin" style="display:none;">आप हमारी सेवाओं के बारे में किसी भी मुद्दे या चिंता के
                            साथ हमसे संपर्क कर सकते हैं। हम आपसे जल्द से जल्द संपर्क करेंगे।</span>
                        <span class="arabic" style="display:none;">يمكنك الاتصال بنا بشأن أي مشكلة أو قلق بشأن خدماتنا.
                            سنتواصل معك في أقرب وقت ممكن.</span>
                        <span class="indonesian" style="display:none;">
                            Anda dapat menghubungi kami dengan masalah atau masalah apa pun terkait layanan kami. Kami
                            akan menghubungi Anda sesegera mungkin.</span>
                        <span class="ban" style="display:none;">আমাদের পরিষেবাদি সম্পর্কিত যে কোনও সমস্যা বা উদ্বেগের
                            সাথে আপনি আমাদের সাথে যোগাযোগ করতে পারেন। আমরা যত তাড়াতাড়ি সম্ভব আপনার সাথে যোগাযোগ
                            করব।</span>
                        <span class="italian" style="display:none;">Puoi contattarci per qualsiasi problema o
                            preoccupazione riguardante i nostri servizi. Ti contatteremo al più presto.</span>
                        <span class="spanish" style="display:none;">
                            Puede contactarnos con cualquier problema o inquietud con respecto a nuestros servicios. Nos
                            pondremos en contacto con usted lo antes posible.</span>
                        <span class="german" style="display:none;">Sie können uns bei Problemen oder Bedenken bezüglich
                            unserer Dienstleistungen kontaktieren. Wir werden uns so schnell wie möglich mit Ihnen in
                            Verbindung setzen.</span>
                        <span class="polish" style="display:none;">
                            Możesz skontaktować się z nami w każdej sprawie lub wątpliwości dotyczącej naszych usług.
                            Skontaktujemy się z Tobą jak najszybciej.</span>
                        <span class="portuguese" style="display:none;">
                            Você pode entrar em contato conosco com qualquer problema ou preocupação sobre nossos
                            serviços. Entraremos em contato o mais breve possível.</span>
                        <span class="mandrin" style="display:none;">如果您对我们的服务有任何问题或疑虑，可以与我们联系。我们会尽快与您联系。</span>
                    </p>
                </div>
            </div> <!-- END row-->
            <div class="row mt-5 ">
                <div class="col-lg-8 mx-auto text-center">
                    <div id="msgholder"></div>
                    <form id="admin_form" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-container">
                                    <i class="fa fa-user bg-icon"></i>
                                    <input class="input-field" type="text" name="name"
                                        value="<?php if ($user->logged_in) echo $user->name;?>" placeholder="Your name"
                                        required>
                                </div>
                                <?php /*<div class="input-group u-rounded-50 border u-of-hidden u-mb-30">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<span class="icon icon-User text-primary"></span>
								</div>
								<input type="text" class="form-control border-0 p-3 input-field" name="name" value="<?php if ($user->logged_in) echo $user->name;?>"
                                placeholder="Your name">
                            </div>*/?>
                        </div>
                        <div class="col-md-6">
                            <div class="input-container">
                                <i class="fa fa-envelope bg-icon"></i>
                                <input class="input-field" type="email" name="email"
                                    value="<?php if ($user->logged_in) echo $user->email;?>" placeholder="Email address"
                                    required>
                            </div>
                            <?php /*<div class="input-group u-rounded-50 border u-of-hidden u-mb-30">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<span class="icon icon-Mail text-primary"></span>
								</div>
								<input type="email" class="form-control border-0 p-3 input-field" name="email" value="<?php if ($user->logged_in) echo $user->email;?>"
                            placeholder="Email address">
                        </div>*/?>
                </div>
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="fas fa-pencil-alt bg-icon"></i>
                        <input type="text" name="subject" class="input-field" placeholder="Subject" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-container">
                        <i class="fa fa-key bg-icon"></i>
                        <input class="input-field" type="text" name="captcha" placeholder="Captcha code" required>
                    </div>
                    <h6 class="text-right"><img src="lib/captcha.php" alt="shipping contact in USA"
                            class="bg-dark p-2 px-3" /></h6>
                    <?php /*<div class="input-group u-rounded-50 border u-of-hidden u-mb-30">
								<div class="input-group-addon bg-white border-0 pl-4 pr-0">
									<img src="lib/captcha.php" alt="" style="color:#36bea6" class="captcha-append" />
								</div>							
								<input class="form-control border-0 p-3 input-field" type="text" name="captcha" placeholder="Captcha code">
							</div>*/?>
                </div>

            </div>
            <textarea class="input-field" name="message" rows="4" placeholder="Your text..."></textarea>
            <button class="btn btn-rounded theme-clr-set u-w-170 mt-4" type="submit" name="doupdate">Submit</button>
            <div id="js-contact-result" data-success-msg="Success, We will get back to you soon"
                data-error-msg="Oops! Something went wrong"></div>
            </form>
        </div> <!-- END col-md-7 -->
        </div> <!-- END row-->
        </div> <!-- END container-->
    </section> <!-- END section-->

    <?php require_once("footer.php");?>

    <div class="scroll-top bg-white box-shadow-v1">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <!-- Validate JS -->
    <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery.ui.touch-punch.js"></script>
    <script src="assets/js/jquery.wysiwyg.js"></script>
    <script src="assets/js/global.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets-theme/js/fury.js"></script>
    <script src="assets/js_/script.js"></script>

    <!-- Validate JS -->
    <script src="assets/js_/validate.js"></script>
    <!-- Contact JS -->
    <script src="assets/js_/contact.js"></script>

    <script src="assets/js_/popper.min.js"></script>
    <script src="assets/js_/bootstrap.min.js"></script>
    <script type="text/javascript">
    // <![CDATA[
    function showResponse(msg) {
        hideLoader();
        if (msg == 'OK') {
            result =
                "<div class=\"bggreen\"><p><span class=\"icon-ok-sign\"><\/span><i class=\"close icon-remove-circle\"></i><span>Thank you!<\/span>Your message has been sent successfully<\/p><\/div>";
            $("#fullform").hide();
        } else {
            result = msg;
        }
        $(this).html(result);
        $("html, body").animate({
            scrollTop: 0
        }, 600);
    }
    $(document).ready(function() {
        var options = {
            target: "#msgholder",
            beforeSubmit: showLoader,
            success: showResponse,
            url: "ajax/sendmail.php",
            resetForm: 0,
            clearForm: 0,
            data: {
                processContact: 1
            }
        };
        $("#admin_form").ajaxForm(options);
    });
    // ]]>
    </script>
</body>

</html>