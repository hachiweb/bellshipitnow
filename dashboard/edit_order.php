<?php
define("_VALID_PHP", true);
require(dirname(__DIR__).'/lib/config.ini.php');

$id = $_POST['id'];
$servername = DB_SERVER;
$username   = DB_USER;
$password   = DB_PASS;
$dbname     = DB_DATABASE;
$conn       = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$product = '';
$query = "select * from order_form where id = $id";
$result=$conn->query($query);
while ($row = $result->fetch_array(MYSQLI_BOTH))  
 {   
     $tracking = $row['tracking'];
    $grandtotal = $row['grandtotal'];
 	$product = json_decode($row['product'], true);
    if($product != '')
    {
        echo 'empty';
    //  echo '<tr><td>';
    $max = sizeOf($product["item"]);
    for($i=0;$i<$max;$i++)
    {
        echo '<tr><td>';
        // echo '</td><td>';
        echo '<input type="text" class="form-control" name="item[]" value="'.$product['item'][$i].'"';
        // echo $product["item"][$i];
        echo '</td><td>';
        echo '<input type="text" class="form-control tracking_id" name="tracking_id" value="'.$row['tracking'].'">';
        echo '</td><td>';
        echo '<input type="text" class="form-control value" name="value[]" value="'.$product['value'][$i].'"';
        echo '</td><td>';
        echo '<input type="text" class="form-control quantity" name="quantity[]" value="'.$product['quantity'][$i].'"';
        echo '</td><td>';
        echo '<select class="form-control" name="category[]"><option value="">Select</option>';
            $query_category = "select * from category";
            $result_query_category = $conn->query($query_category); 
            while ($row = $result_query_category->fetch_array(MYSQLI_BOTH))  
            {
                $sel = '';
                if($row['name_item'] == $product['category'][$i])
                {
                    $sel = 'selected="selected"';
                }
              echo '<option ' . $sel . '>' . $row['name_item'] . '</option>';
            }
        // echo '<input type="text" class="form-control" name="category[]" value="'.$product['category'][$i].'"';
        echo '</td><td>';
        echo '<input type="text" class="form-control" name="product_link[]" value="'.$product['product_link'][$i].'"';
        echo '</td><td>';
        echo '<select class="form-control" name="ship_method[]"><option value="">Select</option>';
            $query_category = "select * from shipping_mode";
            $result_query_category = $conn->query($query_category); 
            while ($row = $result_query_category->fetch_array(MYSQLI_BOTH))  
            {
                $sel = '';
                if($row['ship_mode'] == $product['ship_method'][$i])
                {
                    $sel = 'selected="selected"';
                }
              echo '<option ' . $sel . '>' . $row['ship_mode'] . '</option>';
            }
        // echo '<input type="text" class="form-control" name="ship_method[]" value="'.$product['ship_method'][$i].'"';
        echo '</td><td>';
        if(isset($product['ship_fee'][$i]))
        echo '<input type="text" class="form-control ship_fee" name="ship_fee[]" value="'.$product['ship_fee'][$i].'"';
        else
            echo '<input type="text" class="form-control ship_fee" name="ship_fee[]" value="">';
            echo '</td><td>';
            echo '<input type="text" class="form-control total" name="total[]" value="'.$product['total'][$i].'"></td></tr>'; 
    }
    echo '<tr><td><input type="hidden" id="grand_total" name="grand_total" value="' . $grandtotal. '"></td></tr>';
}
    else
        echo '<tr><td colspan="8"><i align="center" class="display-3 text-warning d-block"><img src="assets/images/alert/ohh_shipment.png" width="130" alt="guarantee a safe and fast shipping to any location" /></i></td></tr>';
        // echo '<tr><td></td><td></td><td></td><td></td><td></td></tr>';

 }

?>