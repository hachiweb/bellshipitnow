<?php
session_start();
// For test payments we want to enable the sandbox mode. If you want to put live
// payments through then this setting needs changing to `false`.
$enableSandbox = false;

// Database settings. Change these for your database configuration.
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https://"; 
else
$link = "http://"; 
$site_url = $link.$_SERVER['HTTP_HOST'];

define("_VALID_PHP", true);
require_once("../../../init.php");

  
  $row = $user->getUserData();
  $customer_number = $row->customer_number;

  $servername = DB_SERVER;
  $username   = DB_USER;
  $password   = DB_PASS;
  $dbname     = DB_DATABASE;
  $db       = new mysqli($servername, $username, $password, $dbname);

  if ($db->connect_error) {
	  die("Connection failed: " . $db->connect_error);
  }

// PayPal settings. Change these to your account details and the relevant URLs
// for your site.
$paypalConfig = [
	'email' => 'bellshipitnow@gmail.com',
	'return_url' => $site_url.'/dashboard/payments/paypal/payment-successful.php',
	'cancel_url' => $site_url.'/dashboard/payments/paypal/payment-cancelled.html',
	'notify_url' => $site_url.'/dashboard/payments/paypal/payments.php'
];

$paypalUrl = $enableSandbox ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr';

// Product being purchased.
if(isset($_POST['item_id']))
{ 
	$_SESSION['cuid']=$_POST['cuid'];
	$itemName = $_POST['item_name'];
	$itemAmount = $_POST['amount'];
	$_SESSION['price']= $itemAmount;
	$_SESSION['item_id']=$_POST['item_id'];
	$_SESSION['tracking-id']=$_POST['tracking-id'];
}
elseif(isset($_POST['shipmet_id']))
{
	$ship_item = $_POST['ship_item'];
	$_SESSION['shipments'] = $_POST['shipments'];
	// echo $ship_item;
	$itemAmount = $_POST['r_costtotal'];
	$_SESSION['r_costtotal']=$itemAmount;
	$_SESSION['shipmet_id']=$_POST['shipmet_id'];
	// echo $_SESSION['shipmet_id'];
	$_SESSION['shipmet-tracking']=$_POST['shipmet-tracking'];
}else{
	//Customer payments products details
	$_SESSION['cus_id']=$_POST['cus_id'];
	$_SESSION['tracking_id']=$_POST['tracking_id'];
	$_SESSION['customer_item_id']=$_POST['customer_item_id'];
	$_SESSION['total_amount']=$_POST['total_amount'];
	$_SESSION['customer_name']=$_POST['customer_name'];

}


// echo "<pre>";
// // echo $_SESSION['customer_item_id'];
//   print_r($_POST);
//   exit;
// Include Functions
require 'functions.php';

// Check if paypal request or response
if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])) {

	// Grab the post data so that we can set up the query string for PayPal.
	// Ideally we'd use a whitelist here to check nothing is being injected into
	// our post data.
	$data = [];
	foreach ($_POST as $key => $value) {
		$data[$key] = stripslashes($value);
	}

	// Set the PayPal account.
	$data['business'] = $paypalConfig['email'];

	// Set the PayPal return addresses.
	$data['return'] = stripslashes($paypalConfig['return_url']);
	$data['cancel_return'] = stripslashes($paypalConfig['cancel_url']);
	$data['notify_url'] = stripslashes($paypalConfig['notify_url']);

	// Set the details about the product being purchased, including the amount
	// and currency so that these aren't overridden by the form data.
	$data['item_name'] = $itemName;
	$data['amount'] = $itemAmount;
	$data['currency_code'] = $_POST['currency_code'];

	// Add any custom fields for the query string.
	//$data['custom'] = USERID;

	// Build the query string from the data.
	$queryString = http_build_query($data);

	// echo '<pre>';
	// print_R($queryString);
	// exit;
	// Redirect to paypal IPN
	header('location:' . $paypalUrl . '?' . $queryString);
	exit();

} else {
	// Handle the PayPal response.

	// Create a connection to the database.
	$db = $con; //new mysqli($dbConfig['host'], $dbConfig['username'], $dbConfig['password'], $dbConfig['name']);

	// Assign posted variables to local data array.
	if(isset($_POST['item_id']) && ($_POST['item_id']!="")){
	$data = [
		'item_name' => $_POST['item_name'],
		'item_number' => $_POST['item_number'],
		'payment_status' => $_POST['payment_status'],
		'payment_amount' => $_POST['mc_gross'],
		'payment_currency' => $_POST['mc_currency'],
		'txn_id' => $_POST['txn_id'],
		'receiver_email' => $_POST['receiver_email'],
		'payer_email' => $_POST['payer_email'],
		'custom' => $_POST['custom'],
	];
	}
	else if(isset($_POST['customer_item_id'])){
	$data = [
		'item_name' => $_POST['customer_name'],
		'item_number' => $_POST['customer_item_id'],
		'payment_status' => $_POST['payment_status'],
		'payment_amount' => $_POST['mc_gross'],
		'payment_currency' => $_POST['mc_currency'],
		'txn_id' => $_POST['txn_id'],
		'receiver_email' => $_POST['receiver_email'],
		'payer_email' => $_POST['payer_email'],
		'custom' => $_POST['custom'],
	];
	}
	else if(isset($_POST['shipmet_id'])){
		$data = [
			'item_name' => $_POST['ship_item'],
			'item_number' => $_POST['shipmet_id'],
			'payment_status' => $_POST['payment_status'],
			'payment_amount' => $_POST['mc_gross'],
			'payment_currency' => $_POST['mc_currency'],
			'txn_id' => $_POST['txn_id'],
			'receiver_email' => $_POST['receiver_email'],
			'payer_email' => $_POST['payer_email'],
			'custom' => $_POST['custom'],
		];
		}
	// We need to verify the transaction comes from PayPal and check we've not
	// already processed the transaction before adding the payment to our
	// database.
	if (verifyTransaction($_POST) && checkTxnid($data['txn_id'])) {
		if (addPayment($data, $email, $userid) !== false) {
			// Payment successfully added.
			 
		}
	}
}
 