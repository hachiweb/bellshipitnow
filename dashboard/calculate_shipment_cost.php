<?php 
$_SERVER["HTTP_ACCEPT_LANGUAGE"]='en-US';
require_once('../vendor/shippo-php-client-master/lib/Shippo.php');
// test key shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c
    // live key shippo_live_2c366c199257cf916d15a08def27ed8d5493621d
Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");
$net_weight = (int)$_POST['r_weight']*(int)$_POST['r_qnty'];

$fromAddress = array(
    'name' => 'Rouzier Charles',
    'street1' => '30719 Tremont Drive',
    'city' => 'Wesley Chapel',
    'state' => 'FL',
    'zip' => 33543,
    'country' => 'United States'
);

$toAddress = array(
    'name' => $_POST['r_name'],
    'street1' => $_POST['r_address'],
    'phone' => $_POST['r_phone'],
    'email' => $_POST['r_email'],
    // 'state' => '',
    'city' => $_POST['r_city'],
    'zip' => $_POST['r_postal'],
    'country' => $_POST['r_dest']
);

$parcel = array(
    'length' => $_POST['length'],
    'provider' => $_POST['courier'],
    'quantity' => $_POST['r_qnty'],
    'width' => $_POST['width'],
    'height' => $_POST['height'],
    'weight'=> $net_weight,
    'distance_unit'=> 'in',
    'mass_unit'=> 'lb',
    'value_amount' => $_POST['r_custom'],
);

 $customs_item = array(
    'description' => $_POST['itemcat'],
    'quantity' => $_POST['r_qnty'],
    'net_weight' => $net_weight,
    'mass_unit' => 'lb',
    'value_amount' => $_POST['r_custom'],
    'value_currency' => 'USD',
    'origin_country' => 'US',
    'tariff_number' => '',
);
// Creating the Customs Declaration
// The details on creating the CustomsDeclaration is here: https://goshippo.com/docs/reference#customsdeclarations
$customs_declaration = Shippo_CustomsDeclaration::create(
array(
    'contents_type'=> 'MERCHANDISE',
    'contents_explanation'=> $_POST['itemcat'],
    'non_delivery_option'=> 'RETURN',
    'certify'=> 'true',
    'certify_signer'=> 'Rouzier Charles',
    'items'=> array($customs_item),
));

try{
    $shipment = Shippo_Shipment::create( array(
        'provider' => $_POST['courier'],
        'address_from'=> $fromAddress,
        'address_to'=> $toAddress,
        'parcels'=> array($parcel),
        'duration_terms'=> $_POST['deli_time'],
        'customs_declaration' => $customs_declaration -> object_id,
        'async'=> false,
        )
    );

    $shipObj = json_decode($shipment);
    $flag1 = 0;
    $flag2 = 0;
    $found = 0;
    // foreach($shipObj->rates as $key => $value){
    //     if (!empty(strrchr($value->provider,$_POST['courier']))) {
    //        $amount[] = $value->amount; 
    //        $object_id[] = $value->object_id; 
    //        $flag1 = 1;
    //        $found = 1;
    //      }
    //      else{
    //          $amount2[] = $value->amount;
    //          $other_object_id =  $value->object_id;
    //          $flag2= 1;
    //      }
    //     //  print_r($value->provider);
    // }
    // if($flag1!=0 && $found==1){
    //     $rate['amount'] = min($amount);
    //     $rate['object_id'] = $object_id[0];
    //     $response = array($rate['amount'],$rate['object_id']);
    //     echo json_encode($response);
    // }
    // else if($flag2!=0 && $flag1==0){
    //     echo min($amount2);
    // }
    // else if($flag1!=0 && $flag2!=0){
    //     echo min(min($amount),min($amount2));
    // }
    // echo  $shipment;

    foreach($shipObj->rates as $key => $value){
        $rate['amount'][] = $value->amount;
        $rate['currency'][] = $value->currency;
        $rate['object_id'][] = $value->object_id;
    }
    $min_key = array_keys($rate['amount'], min($rate['amount']))[0]; 
    if(!empty($min_key)){
        $response = array($rate['amount'][$min_key],$rate['object_id'][$min_key]);
        echo json_encode($response);
    }
    else{
        echo '';
        print_r($shipObj);
    }
    // print_r($shipObj);
    // print_R($shipObj->rates);
}
catch(Exception $e){
    $session['message'] = $e;
    echo $e;
}

