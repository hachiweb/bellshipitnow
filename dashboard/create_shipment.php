<?php 
    $_SERVER["HTTP_ACCEPT_LANGUAGE"]='en-US';
    require_once('../vendor/shippo-php-client-master/lib/Shippo.php');
    // test key shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c
    // live key shippo_live_2c366c199257cf916d15a08def27ed8d5493621d
    Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");

    $object_id = $_POST['object_id'];//'21ecdea108d44a27ba6b6c0e78823702'; //
    // Purchase the desired rate.
    $transaction = Shippo_Transaction::create( array( 
        'rate' => '$object_id', 
        'label_file_type' => "PDF", 
        'async' => false ) );

    // Retrieve label url and tracking number or error message
    if ($transaction["status"] == "SUCCESS"){
        // echo( $transaction["label_url"] );
        // echo("\n");
        // echo( $transaction["tracking_number"] );
        $res = array($transaction["label_url"],$transaction["tracking_number"]);
        echo json_encode($res);

    }else {
        foreach ($transaction['messages'] as $message) {
            echo "--> " . $message . "\n";
        }
    }